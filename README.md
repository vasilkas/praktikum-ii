# Software Development Methods Library
**This repository contains the SDMLibrary Web-Application *v.5* along with its tests.**
# Current version (v.5)
&nbsp;&nbsp;&nbsp;&nbsp;**SDM** - *Software development method*
## Functionalities:
-  Displaying different SDM
-  Adding new SDM
-  Deleting created SDM
-  Detailed view of each SDM
-  Changing details of added method
-  Registration and login
-  Short quiz for deciding an appropriate SDM
-  Filter by categories
-  Responsive design

# Summary
The idea of Software Development Methods Library or **SDML** is having an website that contains all of the information about the most commonly used Software Development Methods.

In **SDML** we cathegorized the different methods of development, so you can have a fast, easy and understandable access to all the information you need. 

The new world of programming along with all the ideas and tasks need a flexible way of development, and we can offer just that. Keeping in mind that the specifications are always changing and that people are keen on trying new ways to work, our hybrid-methods are just the way to solve that problem. **SDML** offers you to invent a new method that is based on "*the classics*", and using that in your work circle to upgrade performance, communication and quality of the product. What we also offer, is sharing your method with your team, so everyone can stay up-to-date on what roles everybody has, the artifacts and events with their description, the new workflow etc.

What we also offer is a short quiz, to decide what development model is most suitable for the specifications at your disposal. Our decision-tree model, which we built based on research, can help you make the right decision or at least guide you to the most applicable base method so you can later on make your own using that as your guide.


SDML is a very useful and powerful tool that can help you grow at rapid speed. ***You hold all the pieces to the puzzle, we just help you assemble it in the most efficient way possible.***

# Short Overview
![Index](index.png)
![Method](method.png)
![Quiz](quiz.png)
![Method Example](methodexample.png)

# Use case
![Use case](usecase.png)

# IDE, tools & requirements
- Intellij IDEA Ultimate 2020.1 :latest [Intellij](https://www.jetbrains.com/idea/download/#section=windows)  
- Java [Java 11.0.2](https://download.java.net/java/GA/jdk11/9/GPL/openjdk-11.0.2_windows-x64_bin.zip)
- MongoDB [MongoDB Compass](https://www.mongodb.com/download-center/compass)
- ELK Stack for logging [Elastic Search](https://www.elastic.co/downloads/elasticsearch), [Kibana](https://www.elastic.co/downloads/kibana), [Logstash](https://www.elastic.co/downloads/logstash)
- Spring Boot 2.3.0 [Spring](https://start.spring.io/)

# User interface
Because we only had five weeks for the development, the user interface is a plain Boostrap webpage. We also used a little inspiration from online forms, web-pages and templates to build our own. Our goal was to have user-friendly and simple, and not over-crowded and complex user interface. We also enriched the whole design by adding different images, fonts and we made the whole application responsive, so you can have the same experience no matter the device you are using.
![Categories](categories.png)
# Login and registration
For a better user experience and easier use, we implemented registration and loging in. Our webpage security is configured through a combination of Spring Security and the implementation of Auth0 API.
- Unregistered users can browse through the web page and have the same access as the registered ones - review development methods, view the details, search by category and take a quiz to find the most suitable development model.
- Registered users can add their own (hybrid) method and change it's details. They also have access to their own profile where they can see their information as well as the development methods they are owners of.

# Architecture
Throughout the project we used the classic MVC architectre, built with the help of Spring Boot, along with several RESTful calls. The implementation of the method bodies relies on a view technology (in this case, Thymeleaf) to perform server-side rendering of the HTML. This combination allowed us more flexibility and ability to manipulate with user entered data.
![MVC](mvc.png)

# Instructions for execution
1. Clone our GitLab project. You can find the link to the repository [here](https://gitlab.com/vasilkas/praktikum-ii)
2. Open with any IDE which supports Spring. (prefferably IntelliJ Ultimate).
3. Go to **File -> Project Structure -> Project SDK ->** ```select Java version 11.0.2``` \
&nbsp;&nbsp; **Note:** Wait until the whole project is done with indexing (bottom right corner in IntelliJ) and then run the program. If that doesn't work please follow the steps below.
4. Go to **File -> Settings -> Build, Execution, Deployment -> Build Tools** and check the **Download external annotations for dependencies**
5. In the same page, configure the **Gradle JVM** to ```Java version 1.8``` \
&nbsp;&nbsp; **Note:** *this is an important step, Gradle isn't stable for the newer (11.0.2) version of Java (which is needed for **Auth0**) and if this is not configured, the application* **won't** *start*.
6. Go to **Configurations -> Edit Configurations** and in **Main class** write: ```com.praktikum.SDM.SoftwareDevelopmentMethodsLibrary```
7. Go to **File -> Settings -> Editor -> General -> Auto import** and check: \
&nbsp;&nbsp;&nbsp;&nbsp; - *Add unambiguous imports on the fly* **and** \
&nbsp;&nbsp;&nbsp;&nbsp; - *Optimize imports on the fly (for current project)* \
&nbsp;&nbsp;**Note:** this should take care of all the imports you don't have.
7. **Wait** until the whole project is done indexing (bottom right corner)
7. Run the application
8. The application in listening on port **80** \
&nbsp;&nbsp;&nbsp;&nbsp; - In your browser write **localhost:8080**

# Missing features
- CI/CD
- Decision tree model

# Methods of work
## Kanban
We started the project with clean Kanban, and then we started implementing other methods, or respectively only the (best) most suitable things. Kanban is a methodology with the principle *"Get through as soon as possible"*, which we inherited in our project, keeping in mind that the requirements were changing every week. We devided all issues with approximately the same size and we organized ourselves in a way that helped the project grow at the fastest speed.

## Scrum
We used Scrum as a hybrid with our first choice method: Kanban. Namely, we placed all the known issues on the Product Backlog, and started working from the highest priority downwards. We implemented the Daily Meetings, Reviews and Retrospectives and set the time limit of each Sprint to 1 week.

## Incremental approach
Of course, it happens that in every individual iteration, the work isn't exactly 100% done. That's why we implemented the incremental approach as a way to further improve the existing funcionalities, make changes to them, or completely change the way they work depending on the ideas and requirements from that week.

# Organizational features
The issues were devided into five boards, one board for every week of development. To review the progress of work, click on the following **[link](https://gitlab.com/vasilkas/praktikum-ii/-/issues)**.
- View the progress from the individual weeks in **Boards -> Switch Board**
- View what the milestones were in **Milestones -> All**
![GitLab Issues](issues.png)

# Synchronization with the repository (developers)
Our repository is built with two main branches (master and development) and two subbranches (staticContent and backend). Please follow the next steps when syncronizing local repositories with the main one:
- Pull from the repository multiple times per day.
- Make sure to checkout into a specific branch and pull if changes are needed.
- Push to branches where actual changes were made (ex. if working on frontend push to staticContent).
- **DO NOT** merge other branches into development or master without previous consultation with the team!
- If you're working with multiple developers on the same branch, after every push go to the branch and check if you have overriden (or deleted) someone else's code.
### Further ugrades (third-party developers)
Five weeks is a short period of time to build the completed and rich project we wanted to build. That's why we accept third-party developer upgrades. If you would like to participate, do not hesitate to **[contact us](mailto:vasilka.saklamaeva@student.um.si?subject=[GitLab]%20Cooperation%20Praktikum%20Project)**.

## If you've already contacted us, please follow the next steps:
- Clone the project and **have a local repository ready**.
- **Open a new branch** starting with the functionlity you're developing and then add your name (ex. registration-sam).
- You can open more branches.
- Push **only** to your branches, for any necessary changes we'll inform you.
- **DO NOT** merge the branch before informing us and showing us the increment.
- Make sure that you leave comments in the code, **especially** the functions.
## Useful tutorials (developers)
- [ELK Stack logging](https://www.youtube.com/watch?v=5s9pR9UUtAU)
- [Introduction to Git](https://www.youtube.com/playlist?list=PLjQo0sojbbxVHcVN4h9DMu6U6spKk21uP)
- [GitLab Tutorial](https://www.youtube.com/watch?v=Jt4Z1vwtXT0&list=PLhW3qG5bs-L8YSnCiyQ-jD8XfHC2W1NL_)

![Developers](developers.png)

