var SDMethod = {};
var basicInfo = {};
var artifacts = [];
var roles = [];
var scenarios = [];
var events = [];
var limits = [];
var methodImages = [];
var roleImage;
var artifactImage;

/*VARS USED TO COLLECT SELETED DATA*/
var tagsChecked = []
var eventHasArtifacts = []
var roleHasEvents = []
var roleHasArtifacts = []
var scenarioHasEvents = []


/*GET BASIC INFO*/
function getBasicInfo() {
    var tagsCheckedClone = JSON.parse(JSON.stringify(tagsChecked));
    var methodImagesClone = JSON.parse(JSON.stringify(methodImages));

    basicInfo = {
        name: $("#methodName").val(),
        description: $("#methodDescription").val(),
        tags: tagsCheckedClone,
        externalLinks: getAndDivideTextInRows('methodExternalLinks'),
        internalLinks: $('#methodInternalLinks').val(),
        advantages: getAndDivideTextInRows('methodAdvantages'),
        disadvantages: getAndDivideTextInRows('methodDisadvantages'),
        requirements: getAndDivideTextInRows('methodRequirements'),
        images: methodImagesClone,
    };
    addBasicInfo();
    console.log(basicInfo);
    tagsChecked.length = 0;
    methodImages.length = 0;
}

function getArtifact() {
    var isMandatory;
    if ($('#methodArtifactMandatory').is(':checked')) {
        isMandatory = true;
    } else {
        isMandatory = false;
    }
    document.getElementById("artifactdiv").setAttribute("style", "border: 1px solid #53868B");
    document.getElementById("artifacthidden").setAttribute("style", "");
    var artifact = {
        id: artifacts.length,
        name: $("#methodArtifactName").val(),
        description: $("#methodArtifactDescription").val(),
        isMandatory: isMandatory,
        checklist: getAndDivideTextInRows("methodArtifactChecklist"),
        image: artifactImage
    };
    artifacts[artifacts.length] = artifact;
    appendComponent(artifact, 'artifact');
    console.log(artifacts);
    artifactImage = null;
}

function getEvent() {
    var isMandatory;
    if ($('#methodEventMandatory').is(':checked')) {
        isMandatory = true;
    } else {
        isMandatory = false;
    }
    document.getElementById("eventdiv").setAttribute("style", "border: 1px solid #53868B");
    document.getElementById("eventhidden").setAttribute("style", "");
    var event = {
        id: events.length,
        name: $("#methodEventName").val(),
        description: $("#methodEventDescription").val(),
        type: $("#methodEventType").val(),
        isMandatory: isMandatory,
        duration: $("#methodEventDuration").find(":selected").val(),
        goal: getAndDivideTextInRows('methodEventGoal'),
        checklist: getAndDivideTextInRows('methodEventChecklist'),
        attachments: getAndDivideTextInRows('methodEventAttachments')
    };
    events[events.length] = event;
    appendComponent(event, 'event');
    console.log(events);
}

function getRole() {
    document.getElementById("rolediv").setAttribute("style", "border: 1px solid #53868B");
    document.getElementById("rolehidden").setAttribute("style", "");
    var roleHasArtifactsClone = JSON.parse(JSON.stringify(roleHasArtifacts));
    var roleHasEventsClone = JSON.parse(JSON.stringify(roleHasEvents));
    console.log(roleHasArtifacts);
    var role = {
        id: roles.length,
        name: $("#methodRoleName").val(),
        description: $("#methodRoleDescription").val(),
        requiredKnowledge: getAndDivideTextInRows('methodRoleKnowledge'),
        canBeCombinedWith: $('#methodRoleCanBeCombinedWithRoles').val(),
        artifactsResponsibility: roleHasArtifactsClone,
        eventsResponsibility: roleHasEventsClone,
        checklist: getAndDivideTextInRows('methodRoleChecklist'),
        attachments: getAndDivideTextInRows('methodRoleAttachments'),
        image: roleImage
    };
    roles[roles.length] = role;
    appendComponent(role, 'role');
    console.log(roles);
    roleHasArtifacts.length = 0;
    roleHasEvents.length = 0;
    roleImage = null;
}

function getScenario() {
    document.getElementById("scenariodiv").setAttribute("style", "border: 1px solid #53868B");
    document.getElementById("scenariohidden").setAttribute("style", "");
    var scenarioHasEventsClone = JSON.parse(JSON.stringify(scenarioHasEvents));
    var scenario = {
        id: scenarios.length,
        name: $("#methodScenarioName").val(),
        steps: scenarioHasEventsClone
    };
    scenarios[scenarios.length] = scenario;
    appendComponent(scenario, 'scenario');
    console.log(scenarios);
    scenarioHasEvents.length = 0;
}

function getLimit() {
    document.getElementById("limitdiv").setAttribute("style", "border: 1px solid #53868B");
    document.getElementById("limithidden").setAttribute("style", "");
    var limit = {
        name: $("#methodLimitName").val(),
        description: $("#methodLimitDescription").val(),
        advantages: getAndDivideTextInRows('methodLimitAdvantages')
    };
    limits[limits.length] = limit;
    appendComponent(limit, 'limit');
    console.log(limits);
}

/*DIVIDE TEXT IN ROWS -- DELIMITER: ; */
function getAndDivideTextInRows(Id) {
    var plainText = document.getElementById(Id).value;
    var formattedText = [];
    var currentIndex = 0;
    var currentWord = "";
    for (var i = 0; i < plainText.length; i++) {
        if (plainText[i] != ";")
            currentWord += plainText[i];
        else {
            formattedText[currentIndex] = currentWord;
            currentIndex++;
            currentWord = "";
        }
    }
    return formattedText;
}

/*FILL SELECT TAGS*/
function fillSelect(id, array) {
    var select = document.getElementById(id);
    for (var i = 0; i < array.length; i++) {
        var opt = array[i].name;
        var el = document.createElement("option");
        el.textContent = opt;
        el.value = opt;
        select.add(el);
    }
}

/*ERASING <option> FROM <select> ON SELECTION AND ADDING A NEW BADGE IN A TAG with id=divID*/
function addBadge(element, divID, array) {
    var btn;
    var opt = $(element).find(":selected");
    if (opt.val() !== "empty") {
        opt.remove();
        var text = opt.val();
        manageValuesToProperArrays(array, text, 'add');
        $("#" + divID).append("<span id=\"" + text + "\" class=\"badge badge-primary\" style='background-color: #53868B;'>\n" +
            "  <span>" + text + "</span>\n" +
            "  <a id=\"" + text + "1\"><i class=\"fas fa-minus ml-2\"></i></a> \n" +
            "</span>   ");
        btn = document.getElementById(text + "1");
    }
    btn.addEventListener('click', function () {
        manageValuesToProperArrays(array, text, 'remove');
        $("#" + element.id).append(new Option(text, text));
        var elem = document.getElementById(text);
        elem.parentNode.removeChild(elem);
    });

};

/*DECIDE TO GIVE VALUE TO ONE OF THE ARRAYS THAT STORE SELECTED BADGES -- METHOD IS USED IN addBadge() */

/*MUST BE CHANGED !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
function manageValuesToProperArrays(array, text, command) {
    if (array === 'tags') {
        if (command === 'add')
            tagsChecked[tagsChecked.length] = text;
        else if (command === 'remove')
            tagsChecked.splice(tagsChecked.indexOf(text), 1);
    } else if (array === 'eventArtifacts') {
        if (command === 'add') {
            var eventArtifact = {artifactType: $("#methodEventArtifactType").val(), artifact: text}
            eventHasArtifacts[eventHasArtifacts.length] = eventArtifact;
        } else if (command === 'remove')
            eventHasArtifacts.splice(eventHasArtifacts.findIndex(x => x.artifact === text), 1);
    } else if (array === 'roleArtifacts') {
        if (command === 'add') {
            console.log("da");
            var roleArtifact = {responsibility: $("#methodRoleArtifactResponsibility").val(), artifact: text};
            console.log(roleArtifact);
            roleHasArtifacts[roleHasArtifacts.length] = roleArtifact;
            console.log(roleHasArtifacts);
        } else if (command === 'remove')
            roleHasArtifacts.splice(roleHasArtifacts.findIndex(x => x.artifact === text), 1);
    } else if (array === 'roleEvents') {
        if (command === 'add') {
            var roleEvent = {responsibility: $("#methodRoleEventResponsibility").val(), event: text};
            roleHasEvents[roleHasEvents.length] = roleEvent;
        } else if (command === 'remove')
            roleHasEvents.splice(roleHasEvents.findIndex(x => x.event === text), 1);
    } else if (array === 'scenarioEvents') {
        if (command === 'add') {
            var scenarioEvent = {
                order: $("#methodScenarioOrder").val(),
                label: $("#methodScenarioLabel").val(),
                event: text
            };
            scenarioHasEvents[scenarioHasEvents.length] = scenarioEvent;
        } else if (command === 'remove')
            scenarioHasEvents.splice(scenarioHasEvents.findIndex(x => x.event === text), 1);
    }
}

function appendComponent(component, componentType) {
    var deleteButton;
    var showButton;
    // $("#print"+componentType).append("<div class='col-sm-3'><span id='added"+componentType+component.id+"'><b>ID: </b>"+component.id+"<br/><span><b>Name: </b>"+component.name+"</span><br/><button type=\"button\" id='delete"+componentType+component.id+"' class=\"btn btn-outline-danger\">Delete <i class=\"fas fa-trash\"></i></button><button type=\"button\" id='show"+componentType+component.id+"' class=\"btn btn-outline-info\">Show <i class=\"fa fa-file-text\"></i></button></span></div>");
    $("#print" + componentType).append("<tr id='added" + componentType + component.id + "'><td>" + component.id + "</td><td>" + component.name + "</td><td><button type=\"button\" id='delete" + componentType + component.id + "' class=\"btn btn-outline-danger\">Delete <i class=\"fas fa-trash\"></i></button></td><td><button type=\"button\" id='show" + componentType + component.id + "' class=\"btn btn-outline-info\">Show <i class=\"fa fa-file-text\"></i></button></td></tr>");
    var addedComponent = document.getElementById("added" + componentType + component.id);
    deleteButton = document.getElementById("delete" + componentType + component.id);
    showButton = document.getElementById("show" + componentType + component.id);
    deleteButton.addEventListener('click', function () {
        findAppropriateList(componentType, component.id);
        addedComponent.parentNode.removeChild(addedComponent);
    });
    showButton.addEventListener('click', function () {
        findAppropriateModal(componentType, component.id);
    });
    /*EDIT BTN*/
    /*<button type=\"button\" id='edit"+componentType+component.id+"' class=\"btn btn-outline-info\">Edit <i class=\"fas fa-pen\"></i></button>*/
}

/**
 *
 * @param type gives us the declaration of the element we want to preview
 * @param id gives us the exact example we wanna preview
 *
 */
function findAppropriateModal(type, id) {
    if (type === 'artifact') {
        var checklist = [];
        $("#artifactInfoImage").empty();
        var index = artifacts[artifacts.findIndex(x => x.id === id)];
        $("#artifactInfoName").text(index.name);
        $('#artifactInfoDescription').text(index.description);
        if (index.isMandatory == true) {
            $("#artifactInfoMandatory").text("This artifact is mandatory!");
        } else {
            $("#artifactInfoMandatory").text("This artifact is optional!")
        }
        $.each(index.checklist, function () {
            checklist = checklist + this + ';';
        });
        $('#artifactInfoChecklist').text(checklist);
        var images_div = "<img src=\"" + index.image + "\" height=\"200px\" width=\"400px\"\>";
        $('#artifactInfoImage').append(images_div);
        $("#artifactInfoModal").modal();
    }
    if (type === 'event') {
        var checklist = [];
        var attachments = [];
        var goal = [];
        var index = events[events.findIndex(x => x.id === id)];
        $("#eventInfoName").text(index.name);
        $('#eventInfoDescription').text(index.description);
        $('#eventInfoDuration').text(index.duration);
        $('#eventInfoType').text(index.type);
        if (index.isMandatory == true) {
            $("#eventInfoMandatory").text("This event is mandatory!");
        } else {
            $("#eventInfoMandatory").text("This event is optional!");
        }
        $.each(index.checklist, function () {
            checklist = checklist + this + ';';
        });
        $.each(index.attachments, function () {
            attachments = attachments + this + ';';
        });
        $.each(index.goal, function () {
            goal = goal + this + ';';
        });
        $('#eventInfoChecklist').text(checklist);
        $('#eventInfoGoal').text(goal);
        $('#eventInfoAttachments').text(attachments);
        $("#eventInfoModal").modal();

    }
    if (type === 'role') {
        var checklist = [];
        var attachments = [];
        var canBeCombinedWith = [];
        var requiredKnowledge = [];
        var index = roles[roles.findIndex(x => x.id === id)];
        var images_div = "<img src=\"" + index.image + "\" height=\"200px\" width=\"400px\"\>";
        $('#roleInfoImage').append(images_div);
        $('#roleInfoName').text(index.name);
        $('#roleInfoDescription').text(index.description);
        if (index.isMandatory == true) {
            $("#roleInfoMandatory").text("This role is mandatory!");
        } else {
            $("#roleInfoMandatory").text("This role is optional!");
        }
        $.each(index.checklist, function () {
            checklist = checklist + this + ';';
        });
        $.each(index.attachments, function () {
            attachments = attachments + this + ';';
        });
        $.each(index.canBeCombinedWith, function () {
            canBeCombinedWith = canBeCombinedWith + this + ';';
        });
        $.each(index.artifactsResponsibility, function () {
            var artifactResponsibilityText = "<span class=\"inline-icons\">\n" +
                "<i class=\"far fa-check-square\" style='color: #A78C7E'></i><span>" + this.responsibility + " the " + this.artifact + " </span></span>";
            $('#roleInfoResponsibilityArtifact').append(artifactResponsibilityText);

        });
        $.each(index.eventsResponsibility, function () {
            var eventResponsibilityText = "<span class=\"inline-icons\">\n" +
                "                                            <i class=\"far fa-check-square\" style='color: #A78C7E'></i><span>" + this.responsibility + " in the " +
                "" + this.event + " </span></span>";
            $('#roleInfoResponsibilityEvent').append(eventResponsibilityText);
        });
        $.each(index.requiredKnowledge, function () {
            requiredKnowledge = requiredKnowledge + this + ';';
        });
        $('#roleInfoChecklist').text(checklist);
        $('#roleInfoAttachments').text(attachments);
        $('#roleInfoCanBeCombinedWith').text(canBeCombinedWith);
        $('#roleInfoRequiredKnowledge').text(requiredKnowledge);
        $("#roleInfoModal").modal();
    }
    if (type === 'scenario') {
        var index = scenarios[scenarios.findIndex(x => x.id === id)];
        $("#scenarioInfoScenarioSteps").empty();
        $('#scenarioInfoName').text(index.name);
        $.each(index.steps, function () {
            var scenarioStepText = "<p>" + this.order + "." + this.label + "-->" + this.event + "</p>";
            $('#scenarioInfoScenarioSteps').append(scenarioStepText);
        });
        $("#scenarioInfoModal").modal();

    }
    if (type === 'limit') {
        var advantages = [];
        var index = limits[limits.findIndex(x => x.id === id)];
        $('#limitInfoName').text(index.name);
        $('#limitInfoDescription').text(index.description);
        $.each(index.advantages, function () {
            advantages = advantages + this + ";";
        });
        $("#limitInfoAdvantages").text(advantages);
        $("#limitInfoModal").modal();

    }

}

function findAppropriateList(componentType, id) {
    if (componentType === 'artifact') {
        console.log("DELETE artifact");
        console.log(artifacts[artifacts.findIndex(x => x.id === id)]);
        artifacts.splice(artifacts.findIndex(x => x.id === id), 1);
    }
    if (componentType === 'event') {
        console.log("DELETE event");
        console.log(events[events.findIndex(x => x.id === id)]);
        events.splice(events.findIndex(x => x.id === id), 1);
    }
    if (componentType === 'role') {
        console.log("DELETE role");
        console.log(roles[roles.findIndex(x => x.id === id)]);
        roles.splice(roles.findIndex(x => x.id === id), 1);
    }
    if (componentType === 'scenario') {
        console.log("DELETE scenario");
        console.log(scenarios[scenarios.findIndex(x => x.id === id)]);
        scenarios.splice(scenarios.findIndex(x => x.id === id), 1);
    }
    if (componentType === 'limit') {
        console.log("DELETE limit");
        console.log(limits[limits.findIndex(x => x.id === id)]);
        limits.splice(limits.findIndex(x => x.id === id), 1);
    }
}


function addBasicInfo() {
    document.getElementById("basicdiv").setAttribute("style", "border: 1px solid #53868B");
    document.getElementById("printbasic").innerHTML = "<h3 class=\"font-weight-light\" style=\"font-size: 20px;\"><b>Name: </b>" + basicInfo.name + "<br/>" + "<b>Description: </b>" + basicInfo.description + "<br/><b>Tags: </b></h3>";
    basicInfo.tags.forEach(tag => document.getElementById("printbasic").innerHTML += tag + ", ");

}

/**
 *
 * @param addedImage the image we wanna add to the role object
 */
function addImageRole(addedImage) {
    if (addedImage.files && addedImage.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            var images_div = "<img src=\"" + e.target.result + "\" height=\"200px\" width=\"400px\"\>";
            $('#methodRoleImage').append(images_div);
            roleImage = e.target.result;

        }
        reader.readAsDataURL(addedImage.files[0]);
    }
}

/**
 *
 * @param addedImage the image we wanna add to the artifact object
 */
function addImageArtifact(addedImage) {
    if (addedImage.files && addedImage.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            var images_div = "<img src=\"" + e.target.result + "\" height=\"200px\" width=\"400px\"\>";
            $('#methodArtifactImage').append(images_div);
            artifactImage = e.target.result;
        }
        reader.readAsDataURL(addedImage.files[0]);
    }
}

/**
 *
 * @param addedImage one(of multiple allowed) image we wanna add to the SDMethod document
 */
function addImageSDMethod(addedImage) {
    if (addedImage.files && addedImage.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            var images_div = "<img src=\"" + e.target.result + "\" height=\"200px\" width=\"400px\"\>";
            $('#methodImages').append(images_div);
            methodImages[methodImages.length] = e.target.result;
        }
        reader.readAsDataURL(addedImage.files[0]);
    }
}

/**
 * checks if scenarios, basic info and artifacts are present before adding the new method
 */
function validateAll() {
    saveMethod();
   /* var validatingAll = 0;
    if (artifacts.length == 0 || scenarios.length == 0 || (Object.keys(basicInfo).length === 0 && basicInfo.constructor === Object)) {
        alert("Every method must have basic info, artifacts and scenarios");
        validatingAll++;
    }
    if (validatingAll === 0) {

    }*/
}

$(document).ready(function () {
    sessionStorage.setItem("token", token);
    console.log("Saved TOKEN " + sessionStorage.getItem("token"));
});

/**
 * creating the SDMethod object and making a REST call using ajax for saving in into MongoDB
 */
function saveMethod() {
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date + ' ' + time;
    var access = JSON.parse(JSON.stringify(contributors));
    access[access.length] = $("#methodAccess").text();
    var SDMethod = {
        name: basicInfo.name,
        description: basicInfo.description,
        tags: basicInfo.tags,
        externalLinks: basicInfo.externalLinks,
        internalLinks: basicInfo.internalLinks,
        advantages: basicInfo.advantages,
        disadvantages: basicInfo.disadvantages,
        requirements: basicInfo.requirements,
        artifacts: artifacts,
        events: events,
        roles: roles,
        scenarios: scenarios,
        limits: limits,
        images: methodImages,
        access: access,
        date: dateTime
    };
    console.log(SDMethod);
    $.ajax({
        type: "POST",
        url: "postSDMethod",
        headers: {
            "Authorization": sessionStorage.getItem("token"),
            "Access-Control-Allow-Methods": "POST, GET ,PUT"
        },
        dataType: 'json',
        data: JSON.stringify(SDMethod),
        success: function (data) {
            console.log("POST API RESPONSE::" + data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("FAILED");
        }
    });
}

/**
 * when the modal is closed reset it
 */
$(document).ready(function () {
    $("#limitsDialog").on("hidden.bs.modal", function () {
        $(this).find('form').trigger('reset');
    });
    $("#artifactDialog").on("hidden.bs.modal", function () {
        $(this).find('form').trigger('reset');
        $("#methodArtifactImage").empty();

    });
    $("#rolesDialog").on("hidden.bs.modal", function () {
        $(this).find('form').trigger('reset');
        $('#roleArtifactBadges').empty();
        $("#roleEventBadges").empty();
        $('#methodRoleImage').empty();
    });
    $("#eventDialog").on("hidden.bs.modal", function () {
        $(this).find('form').trigger('reset');
    });
    $("#scenariosDialog").on("hidden.bs.modal", function () {
        $(this).find('form').trigger('reset');
        $('#scenarioEventBadges').empty();

    });
    $("#artifactInfoModal").on("hidden.bs.modal", function () {
        $(this).find('form').trigger('reset');
        $('#artifactInfoImage').empty();
    });
    $("#roleInfoModal").on("hidden.bs.modal", function () {
        $(this).find('form').trigger('reset');
        $('#roleInfoResponsibilityArtifact').empty();
        $('#roleInfoResponsibilityEvent').empty();
        $('#roleInfoImage').empty();

    });
    $("#eventInfoModal").on("hidden.bs.modal", function () {
        $(this).find('form').trigger('reset');

    });
    $("#scenarioInfoModal").on("hidden.bs.modal", function () {
        $(this).find('form').trigger('reset');
        $('#scenarioInfoScenarioSteps').empty();

    });
    $("#limitInfoModal").on("hidden.bs.modal", function () {
        $(this).find('form').trigger('reset');
        $('#limitInfoAdvantages').empty();


    });

});

/**
 * validating, making sure that the users inputs match our preferable standards
 */
function validateBasicInfo() {
    var fullValidation = 0;
    if ($("#methodName").val() == "") {
        $("#methodName").get(0).placeholder = "Please fill out the name!";
        $("#methodName").css('border', '5px inset #4d4dff');
        fullValidation = fullValidation + 1;
    }
    if ($('#methodDescription').val() == "") {
        $("#methodDescription").get(0).placeholder = "Please fill out the description!";
        $("#methodDescription").css('border', '5px inset #4d4dff');
        fullValidation = fullValidation + 1;
    }
    if ($('#methodExternalLinks').val() != "" && $('#methodExternalLinks').val().substring($('#methodExternalLinks').val().length - 1, $('#methodExternalLinks').val().length) != ';') {
        $("#methodExternalLinks").css('border', '5px inset #4d4dff');
        $("#methodExternalLinks").get(0).placeholder = "Please add ';' at the end!";
        fullValidation = fullValidation + 1;
    }
    if ($('#methodAdvantages').val() != "" && $('#methodAdvantages').val().substring($('#methodAdvantages').val().length - 1, $('#methodAdvantages').val().length) != ';') {
        $("#methodAdvantages").get(0).placeholder = "Please add ';' at the end!";
        $("#methodAdvantages").css('border', '5px inset #4d4dff');
        fullValidation = fullValidation + 1;

    }
    if ($('#methodDisadvantages').val() != "" && $('#methodDisadvantages').val().substring($('#methodDisadvantages').val().length - 1, $('#methodDisadvantages').val().length) != ';') {
        $("#methodDisadvantages").get(0).placeholder = "Please add ';' at the end!";
        $("#methodDisadvantages").css('border', '5px inset #4d4dff');
        fullValidation = fullValidation + 1;

    }
    if ($('#methodRequirements').val() != "" && $('#methodRequirements').val().substring($('#methodRequirements').val().length - 1, $('#methodRequirements').val().length) != ';') {
        $("#methodRequirements").get(0).placeholder = "Please add ';' at the end!";
        $("#methodRequirements").css('border', '5px inset #4d4dff');
        fullValidation = fullValidation + 1;
    } else if (fullValidation == 0) {
        getBasicInfo();
        $('#basicInfoDialog').modal('hide');
    }
}

/**
 * validating, making sure that the users inputs match our preferable standards
 */
function validateArtifact() {
    $("#methodArtifactValidationMandatory").empty();
    var fullValidation = 0;
    if ($("#methodArtifactName").val() == "") {
        $("#methodArtifactName").get(0).placeholder = "Please fill out the name!";
        $("#methodArtifactName").css('border', '5px inset #4d4dff');
        fullValidation = fullValidation + 1;
    }
    if ($('#methodArtifactDescription').val() == "") {
        $("#methodArtifactDescription").get(0).placeholder = "Please fill out the description!";
        $("#methodArtifactDescription").css('border', '5px inset #4d4dff');
        fullValidation = fullValidation + 1;
    }
    if ($('#methodArtifactChecklist').val() != "" && $('#methodArtifactChecklist').val().substring($('#methodArtifactChecklist').val().length - 1, $('#methodArtifactChecklist').val().length) != ';') {
        $("#methodArtifactChecklist").get(0).placeholder = "Please add ';' at the end!";
        $("#methodArtifactChecklist").css('border', '5px inset #4d4dff');
        fullValidation = fullValidation + 1;
    }
    if ($('input[name="artifactMandatory"]:checked').length != true) {
        $("#methodArtifactValidationMandatory").text("You have to choose one!");
        document.getElementById('methodArtifactValidationMandatory').style.color = '#4d4dff';
        fullValidation = fullValidation + 1;
    } else if (fullValidation == 0) {
        getArtifact();
        $('#artifactDialog').modal('hide');
    }
}

/**
 * validating, making sure that the users inputs match our preferable standards
 */
function validateEvent() {
    $("#methodEventValidationMandatory").empty();
    var fullValidation = 0;
    if ($("#methodEventName").val() == "") {
        $("#methodEventName").get(0).placeholder = "Please fill out the name!"
        $('#methodEventName').css('border', '5px inset #4d4dff');
        fullValidation = fullValidation + 1;
    }
    if ($('#methodEventDescription').val() == "") {
        $("#methodEventDescription").get(0).placeholder = "Please fill out description!";
        $("#methodEventDescription").css('border', '5px inset #4d4dff');
        fullValidation = fullValidation + 1;
    }
    if ($('#methodEventType').val() == "") {
        $("#methodEventType").get(0).placeholder = ("Please fill out the type!");
        $("#methodEventType").css('border', '5px inset #4d4dff');
        fullValidation = fullValidation + 1;
    }
    if ($('input[name="eventMandatory"]:checked').length != true) {
        $("#methodEventValidationMandatory").text("You have to choose one!");
        document.getElementById('methodEventValidationMandatory').style.color = '#4d4dff';
        fullValidation = fullValidation + 1;
    }
    if ($('#methodEventAttachments').val() != "" && $('#methodEventAttachments').val().substring($('#methodEventAttachments').val().length - 1, $('#methodEventAttachments').val().length) != ';') {
        $("#methodEventAttachments").get(0).placeholder = ("Please add ';' at the end!");
        $("#methodEventAttachments").css('border', '5px inset #4d4dff');
        fullValidation = fullValidation + 1;

    }
    if ($('#methodEventGoal').val() != "" && $('#methodEventGoal').val().substring($('#methodEventGoal').val().length - 1, $('#methodEventGoal').val().length) != ';') {
        $("#methodEventGoal").get(0).placeholder = ("Please add ';' at the end!");
        $("#methodEventGoal").css('border', '5px inset #4d4dff');
        fullValidation = fullValidation + 1;

    }
    if ($('#methodEventChecklist').val() != "" && $('#methodEventChecklist').val().substring($('#methodEventChecklist').val().length - 1, $('#methodEventChecklist').val().length) != ';') {
        $("#methodEventChecklist").get(0).placeholder = ("Please add ';' at the end!");
        $("#methodEventChecklist").css('border', '5px inset #4d4dff');
        fullValidation = fullValidation + 1;
    } else if (fullValidation == 0) {
        getEvent();
        $('#eventDialog').modal('hide');
    }
}

/**
 * validating, making sure that the users inputs match our preferable standards
 */
function validateRole() {
    $("#methodRoleValidationMandatory").empty();
    var fullValidation = 0;
    if ($("#methodRoleName").val() == "") {
        $("#methodRoleName").get(0).placeholder = "Please fill out the name!"
        $("#methodRoleName").css('border', '5px inset #4d4dff');
        fullValidation = fullValidation + 1;
    }
    if ($('#methodRoleDescription').val() == "") {
        $("#methodRoleDescription").get(0).placeholder = "Please fill out the description!"
        $("#methodRoleDescription").css('border', '5px inset #4d4dff');
        fullValidation = fullValidation + 1;
    }
    if ($('input[name="roleMandatory"]:checked').length != true) {
        $("#methodRoleValidationMandatory").text("You have to choose one!");
        document.getElementById('methodRoleValidationMandatory').style.color = '#4d4dff';
        fullValidation = fullValidation + 1;
    }
    if ($('#methodRoleAttachments').val() != "" && $('#methodRoleAttachments').val().substring($('#methodRoleAttachments').val().length - 1, $('#methodRoleAttachments').val().length) != ';') {
        $("#methodRoleAttachments").get(0).placeholder = "Please add ';' at the end!";
        $("#methodRoleAttachments").css('border', '5px inset #4d4dff');
        fullValidation = fullValidation + 1;

    }
    if ($('#methodRoleKnowledge').val() != "" && $('#methodRoleKnowledge').val().substring($('#methodRoleKnowledge').val().length - 1, $('#methodRoleKnowledge').val().length) != ';') {
        $("#methodRoleKnowledge").get(0).placeholder = "Please add ';' at the end!";
        $("#methodRoleKnowledge").css('border', '5px inset #4d4dff');
        fullValidation = fullValidation + 1;

    }
    if ($('#methodRoleChecklist').val() != "" && $('#methodRoleChecklist').val().substring($('#methodRoleChecklist').val().length - 1, $('#methodRoleChecklist').val().length) != ';') {
        $("#methodRoleChecklist").get(0).placeholder = "Please add ';' at the end!";
        $("#methodRoleChecklist").css('border', '5px inset #4d4dff');
        fullValidation = fullValidation + 1;
    } else if (fullValidation == 0) {
        getRole();
        $('#rolesDialog').modal('hide');
    }
}

/**
 * validating, making sure that the users inputs match our preferable standards
 */
function validateScenario() {
    var fullValidation = 0;
    if ($("#methodScenarioName").val() == "") {
        $("#methodScenarioName").get(0).placeholder = "Please fill out the name!";
        $("#methodScenarioName").css('border', '5px inset #4d4dff');
        fullValidation = fullValidation + 1;
    } else if (fullValidation == 0) {
        getScenario();
        $('#scenariosDialog').modal('hide');
    }
}

/**
 * validating, making sure that the users inputs match our preferable standards
 */
function validateLimit() {
    var fullValidation = 0;
    if ($("#methodLimitName").val() == "") {
        $("#methodLimitName").get(0).placeholder = "Please fill out the name!";
        $("#methodLimitName").css('border', '5px inset #4d4dff');
        fullValidation = fullValidation + 1;
    }
    if ($('#methodLimitDescription').val() == "") {
        $("#methodLimitDescription").get(0).placeholder = "Please fill out the description!";
        $("#methodLimitDescription").css('border', '5px inset #4d4dff');
        fullValidation = fullValidation + 1;
    }
    if ($('#methodLimitAdvantages').val() != "" && $('#methodLimitAdvantages').val().substring($('#methodLimitAdvantages').val().length - 1, $('#methodLimitAdvantages').val().length) != ';') {
        $("#methodLimitAdvantages").get(0).placeholder = "Please add ';' at the end!";
        $("#methodLimitAdvantages").css('border', '5px inset #4d4dff');
        fullValidation = fullValidation + 1;

    } else if (fullValidation == 0) {
        getLimit();
        $('#limitsDialog').modal('hide');
    }

}
//list of contributors
let contributors = []
//adds badges for contributors
function addContributors(divID) {
    var text = $("#emailContributor").val();
    $("#emailContributor").val("");
    let btn;
    if (text !== "") {
        contributors[contributors.length] = text;
        $("#" + divID).append("<span id=\"" + text + "\" class=\"badge badge-primary\" style='background-color: #53868B;'>\n" +
            "  <span>" + text + "</span>\n" +
            "  <a id=\"" + text + "1\"><i class=\"fas fa-minus ml-2\"></i></a> \n" +
            "</span>   ");
        btn = document.getElementById(text + "1");
        btn.addEventListener('click', function () {
            console.log("FIND" + contributors.indexOf(text));
            contributors.splice(contributors.indexOf(text), 1);
            var elem = document.getElementById(text);
            elem.parentNode.removeChild(elem);
        });
    }
}
