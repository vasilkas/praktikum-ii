let modelsList;
let points;
let radioGroupNames;

function quizGrading() {
    let optionNumber = 0;
    modelsList = createModelsMap();
    points = createPoints();
    radioGroupNames = getRadioGroupNames();
    for (let i = 0; i < 10; i++) {
        //assigns a specific radioGroup
        let radioGroup = document.getElementById("quizForm").elements[radioGroupNames[i]];
        optionNumber = findOptionNumber(radioGroup);
        givePoints(points[i], optionNumber);
    }
    //PRINTS OUT ALL MODELS WITH POINTS
    for (let [key, value] of modelsList.entries()){
        console.log(key + " : " + value);
        document.getElementById("showMe").setAttribute("style", "")
        document.getElementById("printAnswers").innerHTML+="<h3 class=\"card-title d-inline font-weight-light text-lg-left mt-4 mb-0\">\n" +
            "<b>"+key+" model</b> - score: "+value+"</h3><br/>";
    }

};

//GETS THE SELECTED OPTIONS NUMBER IN THE RADIOGROUP
function findOptionNumber(radioGroup) {
    for (let i = 0; i < radioGroup.length; i++)
        if (radioGroup[i].checked)
            return i;
};

//GRADES EACH MODEL(modelsList) FROM THE MAP WITH THE SELECTED CHOICE(optionNumber) FROM THE 2D LIST(points)
function givePoints(points, optionNumber) {
    let i = 0;
    for (let [key, value] of modelsList.entries()) {
        let point = points[optionNumber][i] + value;
        modelsList.set(key, point);
        i++;
    }
};

//RETURNS A MAP OF ALL MODELS
function createModelsMap() {
    let modelsList = new Map();
    modelsList.set("Waterfall", 0);
    modelsList.set("VModel", 0);
    modelsList.set("Prototyping", 0);
    modelsList.set("Spiral", 0);
    modelsList.set("Iterative", 0);
    modelsList.set("Agile", 0);
    return modelsList;
}

//RETURNS ALL RADIOGROUPS NAMES
function getRadioGroupNames() {
    return ["sizeCompany", "geoWorkspace", "sizeProject", "experience", "component", "scope", "documentation", "requirements", "estTime", "complex"];
}

//RETURNS A 3D-ARRAY
function createPoints() {
    //Question 1
    const points1 = [
        [10, 3, 5, 8, 10, 2],
        [3, 10, 5, 0, 6, 10],
        [5, 5, 5, 5, 5, 5]
    ];
    //Question 2
    const points2 = [
        [10, 10, 5, 8, 10, 10],
        [10, 1, 5, 0, 10, 10],
        [5, 5, 5, 5, 5, 5]
    ];
    //Question 3
    const points3 = [
        [10, 10, 8, 8, 10, 2],
        [0, 0, 3, 0, 3, 10],
        [5, 5, 5, 5, 5, 5]
    ];
    //Question 4
    const points4 = [
        [10, 10, 10, 10, 10, 10],
        [8, 9, 7, 7, 9, 5],
        [6, 4, 0, 0, 9, 3],
        [5, 5, 5, 5, 5, 5]
    ];
    //Question 5
    const points5 = [
        [10, 10, 10, 10, 10, 10],
        [8, 7, 3, 8, 5, 10],
        [10, 0, 5, 10, 5, 3],
        [5, 5, 5, 5, 5, 5]
    ];
    //Question 6
    const points6 = [
        [10, 10, 5, 8, 5, 10],
        [0, 0, 5, 10, 5, 10],
        [5, 5, 5, 5, 5, 5]
    ];
    //Question 7
    const points7 = [
        [0, 0, 5, 0, 10, 10],
        [10, 10, 5, 10, 5, 4],
        [5, 5, 5, 5, 5, 5]
    ];
    //Question 8
    const points8 = [
        [8, 8, 10, 10, 8, 0],
        [3, 3, 2, 2, 3, 10],
        [5, 5, 5, 5, 5, 5]
    ];
    //Question 9
    const points9 = [
        [10, 10, 5, 5, 10, 0],
        [0, 0, 5, 5, 0, 10],
        [5, 5, 5, 5, 5, 5]
    ];
    //Question 10
    const points10 = [
        [10, 10, 0, 0, 10, 0],
        [0, 0, 10, 10, 0, 10],
        [5, 5, 5, 5, 5, 5]
    ];
    return [points1, points2, points3, points4, points5, points6, points7, points8, points9, points10]; // init 3d array
}