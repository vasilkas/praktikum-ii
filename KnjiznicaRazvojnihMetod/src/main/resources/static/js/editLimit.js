/** this function takes the id of a limit and based on that finds the said limit and displays it accordingly
 * in the modal which allows editing
 *
 * @param id
 */

function displayLimitModal(id) {
        $.each(method.limits, function () {
            if (this.id == id) {
                $('#methodLimitId').text(this.id);
                $('#methodLimitName').text(this.name);
                $('#methodLimitDescription').text(this.description);
                $('#methodLimitAdvantages').val(this.advantages);
        }
        });
        $('#limitModal').modal();
}
/**forming a json object for the limit and using ajax to make a REST call which will
 * find and edit the method that limit is in
 */
function editLimit() {
    var limit = {
        id:$("#methodLimitId").text(),
        name: $("#methodLimitName").text(),
        description:$("#methodLimitDescription").val(),
        advantages:$('#methodLimitAdvantages').val()

    }
    var methodName = $("#methodName").text();

    $.ajax({
        type: "POST",
        url: "updateLimit/"+methodName,
        xhrFields: {
            withCredentials: true
        },
        headers: {
            "Content-Type": "application/json"
        },
        dataType:'json',
        data: JSON.stringify(limit),
        success: function (data) {
            console.log("POST API RESPONSE::" + data.name);
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
}
