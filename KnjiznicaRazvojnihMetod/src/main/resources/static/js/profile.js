//save token in sessionStorage for sending it through header
$(document).ready( function(){
    sessionStorage.setItem("token", token);
    console.log("TOKEN SAVED");
});
//deleting method on button click
function deleteMethod(methodName){
    var tableRow = document.getElementById(methodName);
    tableRow.remove();
    $.ajax({
        type: "DELETE",
        url: "deleteMethod",
        crossDomain: true,
        headers: {"Authorization": sessionStorage.getItem("token")},
        dataType: 'json',
        data: methodName,
        success: function (data) {
            console.log("DELETE method by name::" + data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("FAILED");
        }
    });
}
//deleting method on button click
function deleteUser(email){
    var tableRow = document.getElementById(email);
    tableRow.remove();
    $.ajax({
        type: "DELETE",
        url: "deleteUser",
        crossDomain: true,
        headers: {"Authorization": sessionStorage.getItem("token")},
        dataType: 'json',
        data : email,
        success: function (data) {
            console.log("DELETE user by email::" + data);
        },
        error: function () {
            console.log("FAILED");
        }
    });
}