function addRequirement(){
    var requirement = $("#methodAddRequirements").val();
    console.log(requirement);
    requirementsExisting[requirementsExisting.length] = requirement;
    var added = "<span id='methodRequirements"+requirement+"' class=\"inline-icons\"><i class=\"far fa-check-square icon-color\" ></i><span >"+requirement+" <i onclick='deleteElement(\""+requirement+"\",\"methodRequirements\")'  class=\"fas fa-trash icon-color\"></i></span></span></br>";
    $("#methodRequirements").append(added);
}
function addAdvantage(){
    var advantage = $("#methodAddAdvantages").val();
    advantagesExisting[advantagesExisting.length] = advantage;
    var added = "<p id='methodAdvantages"+advantage+"'>"+advantage+" <i onclick='deleteElement(\""+advantage+"\",\"methodAdvantages\")'  class=\"fas fa-trash icon-color\"></i></p>";
    $("#methodAdvantages").append(added);

}
function addDisadvantage(){
    var disadvantage = $("#methodAddDisadvantages").val();
    console.log(disadvantage);
    disadvantagesExisting[disadvantagesExisting.length] = disadvantage;
    var added = "<p id='methodDisadvantages"+disadvantage+"'>"+disadvantage+" <i onclick='deleteElement(\""+disadvantage+"\",\"methodDisadvantages\")'  class=\"fas fa-trash icon-color\"></i></p>";
    $("#methodDisadvantages").append(added);

}
function addExternalLinks(){
    var externalLinks = $("#methodAddExternalLinks").val();
    externalLinksExisting[externalLinksExisting.length] = externalLinks;
    var added = "<p id='methodExternalLinks"+externalLinks+"'>"+externalLinks +" <i onclick='deleteElement(\""+externalLinks +"\",\"methodExternalLinks\")'  class=\"fas fa-trash icon-color\"></i></p>";
    $("#methodExternalLinks").append(added);
}
function addInternalLinks(){
    var internalLinks = $("#methodAddInternalLinks").val();
    internalLinksExisting[internalLinksExisting.length] = internalLinks;
    var added = "<p id='methodInternalLinks"+internalLinks+"'>"+internalLinks +" <i onclick='deleteElement(\""+internalLinks +"\",\"methodInternalLinks\")'  class=\"fas fa-trash icon-color\"></i></p>";
    $("#methodInternalLinks").append(added);

}

/**
 * take the requirements list and using ajax make a REST call to update
 */
function editRequirement(){
    var requirements  = new Array();
    $.each(requirementsExisting, function(index,value){
        console.log(value)
        requirements.push(value);
    });
    var methodName = $("#methodName").text();
    console.log(methodName);
    $.ajax({
        type: "POST",
        url: "updateRequirements/"+methodName,
        dataType:'json',
        traditional:true,
        xhrFields: {
            withCredentials: true
        },
        headers: {
            "Content-Type": "application/json"
        },
        data:JSON.stringify(requirements),
        success: function (data) {
            console.log("POST API RESPONSE::");
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
}
/**
 * take the advantages list and using ajax make a REST call to update
 */
function editAdvantages(){
    var advantages  = new Array();
    $.each(advantagesExisting, function(index,value){
        console.log(value)
        advantages.push(value);
    });
    var methodName = $("#methodName").text();
    console.log(methodName);
    $.ajax({
        type: "POST",
        url: "updateAdvantages/"+methodName,
        dataType:'json',
        traditional:true,
        xhrFields: {
            withCredentials: true
        },
        headers: {
            "Content-Type": "application/json"
        },
        data:JSON.stringify(advantages),
        success: function (data) {
            console.log("POST API RESPONSE::");
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
}
/**
 * take the disadvantages list and using ajax make a REST call to update
 */
function editDisadvantages(){
    var disadvantages  = new Array();
    $.each(disadvantagesExisting, function(index,value){
        console.log(value)
        disadvantages.push(value);
    });
    var methodName = $("#methodName").text();
    console.log(methodName);
    $.ajax({
        type: "POST",
        url: "updateDisadvantages/"+methodName,
        dataType:'json',
        traditional:true,
        xhrFields: {
            withCredentials: true
        },
        headers: {
            "Content-Type": "application/json"
        },
        data:JSON.stringify(disadvantages),
        success: function (data) {
            console.log("POST API RESPONSE::");
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
}
/**
 * take the external links list and using ajax make a REST call to update
 */
function editExternalLinks() {
    var externalLinks  = new Array();
    $.each(externalLinksExisting, function(index,value){
        console.log(value)
        externalLinks.push(value);
    });
    var methodName = $("#methodName").text();
    console.log(methodName);
    $.ajax({
        type: "POST",
        url: "updateExternalLinks/"+methodName,
        dataType:'json',
        traditional:true,
        xhrFields: {
            withCredentials: true
        },
        headers: {
            "Content-Type": "application/json"
        },
        data:JSON.stringify(externalLinks),
        success: function (data) {
            console.log("POST API RESPONSE::");
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
}
/**
 * take the internal links list and using ajax make a REST call to update
 */
function editInternalLinks() {
    var internalLinks  = new Array();
    $.each(internalLinksExisting, function(index,value){
        console.log(value)
        internalLinks.push(value);
    });
    var methodName = $("#methodName").text();
    console.log(methodName);
    $.ajax({
        type: "POST",
        url: "updateInternalLinks/"+methodName,
        dataType:'json',
        traditional:true,
        xhrFields: {
            withCredentials: true
        },
        headers: {
            "Content-Type": "application/json"
        },
        data:JSON.stringify(internalLinks),
        success: function (data) {
            console.log("POST API RESPONSE::");
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
}
/**
 * take the description and using ajax make a REST call to update it
 */
function editDescription() {
    var data ={
        methodName:$("#methodName").text(),
        description:$("#methodDescription").text()
    }
    $.ajax({
        type: "POST",
        url: "updateDescription?"+ $.param(data),
        dataType:'json',
        contentType: "application/json; charset=utf-8",

    });
}

/**
 *
 * @param name the name of the element
 * @param text the id of the div from which we wanna remove an element
 */
function deleteElement(name,text){
    var deletedElement;
    if(text=="methodRequirements"){
        deletedElement = document.getElementById(text+name);
        deletedElement.remove();
        $.each(requirementsExisting, function (index,value) {
            if(name==value){
                requirementsExisting.splice(index,1);
            }
        });
    }
    else if(text=="methodAdvantages"){
        deletedElement = document.getElementById(text+name);
        deletedElement.remove();
        console.log(deletedElement);
        $.each(advantagesExisting, function (index,value) {
            if(name==value){
                advantagesExisting.splice(index,1);
            }
        });

    }
    else if(text=="methodDisadvantages"){
        deletedElement = document.getElementById(text+name);
        deletedElement.remove();
        $.each(disadvantagesExisting, function (index,value) {
            if(name==value){
                disadvantagesExisting.splice(index,1);
            }
        });
    }
    else if(text=="methodExternalLinks"){
        deletedElement = document.getElementById(text+name);
        deletedElement.remove();
        $.each(externalLinksExisting, function (index,value) {
            if(name==value){
                externalLinksExisting.splice(index,1);
            }
        });
    }
    else if(text=="methodInternalLinks"){
        deletedElement = document.getElementById(text+name);
        deletedElement.remove();
        $.each(internalLinksExisting, function (index,value) {
            if(name==value){
                internalLinksExisting.splice(index,1);
            }
        });
    }
}
