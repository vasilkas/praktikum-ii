var image;

/** this function takes the id of an artifact and based on that finds the said artifact and displays it accordingly
 * in the modal which allows editing
 *
 * @param id
 */
function displayArtifactModal(id){
        var checklist = []
        var attachments = []
        $.each(method.artifacts, function () {
            if(this.id==id) {
                $('#methodArtifactName').text(this.name);
                $('#methodArtifactDescription').val(this.description);
                $("#methodArtifactImage").empty();
                $('#methodArtifactId').text(this.id);

                var images_div = "<img src=\"" + this.image + "\" height=\"200px\" width=\"400px\"\>";
                $('#methodArtifactImage').append(images_div);
                image = this.image;
                if (this.mandatory == true) {
                    $("#methodArtifactMandatory").text("This artifact is mandatory!");
                } else {
                    $("#methodArtifactOptional").text("This artifact is optional!");
                }
                $.each(this.checklist, function () {
                    checklist = checklist + this + '/n';
                });
                $.each(this.attachments, function () {
                    attachments = attachments + this + '/n';
                });

                $('#methodArtifactChecklist').val(checklist);
                $('#methodArtifactAttachments').val(attachments);
            }
        });
        $('#artifactModal').modal();
    }

/**
 * forming a json object for the artifact and using ajax to make a REST call which will
 * find and edit the method that artifact is in
 */

function editArtifact() {
    var isMandatory
    if($('#methodArtifactMandatory').is(':checked')){
        isMandatory=true;}
    else {isMandatory=false;}
    var artifact = {
        id:$("#methodArtifactId").text(),
        name: $("#methodArtifactName").text(),
        description: $("#methodArtifactDescription").val(),
        isMandatory: isMandatory,
        image:image,
        checklist: getAndDivideTextInRows("methodArtifactChecklist")
    };
    var methodName = $("#methodName").text();
    //post klic za sprememba artifact v bazo
    $.ajax({
        type: "POST",
        url: "updateArtifact/"+methodName,
        xhrFields: {
            withCredentials: true
        },
        headers: {
            "Content-Type": "application/json"
        },
        dataType:'json',
        data: JSON.stringify(artifact),
        success: function (data) {
            console.log("POST API RESPONSE::" + data.name);
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
}

/**
 *
 * @param addedImage the image we wanna save/replace in the database
 */
function changeImageArtifact(addedImage){
    if (addedImage.files && addedImage.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#methodArtifactImage').empty();
            var images_div = "<img src=\"" + e.target.result + "\" height=\"200px\" width=\"400px\"\>";
            $('#methodArtifactImage').append(images_div);
            console.log(image);
            image = e.target.result;
            console.log(image);

        }
        reader.readAsDataURL(addedImage.files[0]);
    }
}
function getAndDivideTextInRows(Id) {
    var plainText = document.getElementById(Id).value;
    var formattedText = [];
    var currentIndex = 0;
    var currentWord = "";
    for (var i = 0; i < plainText.length; i++) {
        if (plainText[i] != ";")
            currentWord += plainText[i];
        else {
            formattedText[currentIndex] = currentWord;
            currentIndex++;
            currentWord = "";
        }
    }
    return formattedText;
}
