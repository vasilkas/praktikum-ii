var stepsInScenario = [];
/** this function takes the id of a scenario and based on that finds the said scenario and displays it accordingly
 * in the modal which allows editing
 *
 * @param id
 */
function displayScenarioModal(id){
        var stepsList = []
        stepsInScenario.length=0;
        $("#methodExistingScenarioSteps").empty();
        $.each(method.scenarios,function(){
            if(this.id==id) {
                $('#methodScenarioName').text(this.name);
                $('#methodScenarioId').text(this.id);
                $.each(this.steps, function () {
                    var scenarioStepText = "<p id='methodExistingScenarioSteps" + this.label + "'>" + this.order + "." + this.label + "-->" + this.event + " <i onclick='deleteStep(\"" + this.label + "\",\"methodExistingScenarioSteps\")'  class=\"fas fa-trash\"></i></p>";
                    $('#methodExistingScenarioSteps').append(scenarioStepText);
                    var step = {order: this.order, label: this.label, event: this.event};
                    stepsInScenario[stepsInScenario.length] = step;
                });
            }
        });
        $('#scenarioModal').modal();
    }
/**forming a json object for the scenario and using ajax to make a REST call which will
 * find and edit the method that scenario is in
 */
function editScenario() {

    var scenario = {
        id:$("#methodScenarioId").text(),
        name: $("#methodScenarioName").text(),
        steps: stepsInScenario
    }
    var methodName = $("#methodName").text();
    console.log(JSON.stringify(scenario));

    $.ajax({
        type: "POST",
        url: "updateScenario/"+methodName,
        xhrFields: {
            withCredentials: true
        },
        headers: {
            "Content-Type": "application/json"
        },
        dataType:'json',
        data: JSON.stringify(scenario),

        success: function (data) {
            console.log("POST API RESPONSE::" + data.name );
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });


}
/**
 * adding a new step in the div as well as the global list of steps in a scenario
 */
function addStep(){
    var step = {
        order:$("#methodScenarioOrder").children("option:selected").val(),
        label:$("#methodScenarioLabel").val(),
        event:$("#methodScenarioEvent").children("option:selected").val()
    }
    var scenarioStepText ="<p id='methodExistingScenarioSteps"+step.label+"'>"+step.order+"."+step.label+"-->"+step.event+" <i onclick='deleteStep(\""+step.label+"\",\"methodExistingScenarioSteps\")'  class=\"fas fa-trash\"></i></p>";
    $('#methodExistingScenarioSteps').append(scenarioStepText);
    console.log(step);
    stepsInScenario[stepsInScenario.length] = step;
}

/**
 *
 * @param name the name of the element
 * @param text the id of the div from which we wanna remove an element
 */
function deleteStep(name,text) {
var deletedElement;
var element = document.getElementById(text);
deletedElement = document.getElementById(text+name);
console.log(deletedElement);
deletedElement.remove();
$.each(stepsInScenario, function (index,value) {
    if(name==this.label){
        stepsInScenario.splice(index,1);
    }
});}