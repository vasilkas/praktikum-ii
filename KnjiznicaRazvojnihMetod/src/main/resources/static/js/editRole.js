var responsibilitiesEventsRole = [];
var responsibilitiesArtifactsRole = [];
var image;
/** this function takes the id of a role and based on that finds the said role and displays it accordingly
 * in the modal which allows editing
 *
 * @param id
 */
function displayRoleModal(id){
    var checklist = []
    var attachments = []
    var canBeCombinedWith = []
    var requiredKnowledge = []
    responsibilitiesArtifactsRole.length=0;
    responsibilitiesEventsRole.length=0;
    console.log(method.roles);
    console.log(id);
    $.each(method.roles, function () {
        if(this.id==id){
            $('#methodRoleName').text(this.name);
            $('#methodRoleDescription').val(this.description);
            $('#methodRoleId').text(this.id);
            $("#methodRoleImage").empty();
            $("#methodExistingResponsibilityEvent").empty();
            $("#methodExistingResponsibilityArtifact").empty();
            var images_div = "<img src=\"" + this.image + "\" height=\"200px\" width=\"400px\"\>";
            $('#methodRoleImage').append(images_div);
            image = this.image;
            console.log(this.mandatory);
            if (this.mandatory === true) {
                $("#methodRoleMandatory").attr('checked', 'checked');
            } else {
                $("#methodRoleOptional").attr("checked", 'checked');
            }
            $.each(this.checklist, function () {
                checklist = checklist + this + ';';
            });
            $.each(this.attachments, function () {
                attachments = attachments + this + ';';
            });
            $.each(this.canBeCombinedWith, function () {
                canBeCombinedWith = canBeCombinedWith + this + ';';
            });
            $.each(this.artifactsResponsibility, function () {
                var artifactResponsibilityText ="<span id='methodExistingResponsibilityArtifact"+this.artifact+"' class=\"inline-icons\">\n" +
                    "<i class=\"far fa-check-square\" style='color: #A78C7E'></i><span>"+this.responsibility+" the "+this.artifact+" <i onclick='deleteResponsibility(\""+this.artifact+"\",\"methodExistingResponsibilityArtifact\")' class=\"fas fa-trash\"></i></span></span>";
                $('#methodExistingResponsibilityArtifact').append(artifactResponsibilityText);
                var artifact ={responsibility:this.responsibility,artifact:this.artifact};
                responsibilitiesArtifactsRole[responsibilitiesArtifactsRole.length] = artifact;

            });
            $.each(this.eventsResponsibility, function () {
                var eventResponsibilityText ="<span id='methodExistingResponsibilityEvent"+this.event+"' class=\"inline-icons\">\n" +
                    "                                            <i class=\"far fa-check-square\" style='color: #A78C7E'></i><span >"+this.responsibility+" in the " +
                    ""+this.event+" <i onclick='deleteResponsibility(\""+this.event+"\",\"methodExistingResponsibilityEvent\")' class=\"fas fa-trash\"></i></span></span>";
                $('#methodExistingResponsibilityEvent').append(eventResponsibilityText);
                var event ={responsibility:this.responsibility,event:this.event};
                responsibilitiesEventsRole[responsibilitiesEventsRole.length] = event;
            });
            $.each(this.requiredKnowledge, function () {
                requiredKnowledge = requiredKnowledge + this +';';
            });
            $('#methodRoleChecklist').val(checklist);
            $('#methodRoleAttachments').val(attachments);
            $('#methodCanBeCombinedWith').val(canBeCombinedWith);
            $('#methodRoleRequiredKnowledge').val(requiredKnowledge);
        }
    });
    $('#roleModal').modal('show');
    }

/**
 * adding a new artifact responsibility in the div as well as the global list of artifact responsibilities
  */
function addArtifactResponsibility(){
    var artifactResponsibility= {
        responsibility: $("#methodRoleArtifactResponsibility").val(),
        artifact:$("#methodRoleArtifact").children("option:selected").val()
    }
    var artifactResponsibilityText ="<p id='methodExistingResponsibilityArtifact"+artifactResponsibility.artifact+"'>"+artifactResponsibility.responsibility+"-->"+artifactResponsibility.artifact+" <i onclick='deleteResponsibility(\""+artifactResponsibility.artifact+"\",\"methodExistingResponsibilityArtifact\")' class=\"fas fa-trash\"></i></p>";
    $('#methodExistingResponsibilityArtifact').append(artifactResponsibilityText);
    responsibilitiesArtifactsRole[responsibilitiesArtifactsRole.length] = artifactResponsibility;
}
/**
 * adding a new event responsibility in the div as well as the global list of event responsibilities
 */
function addEventResponsibility(){
    var eventResponsibility= {
        responsibility: $("#methodRoleEventResponsibility").val(),
        event:$("#methodRoleEvent").children("option:selected").val()
    }
    var eventResponsibilityText ="<p id='methodExistingResponsibilityEvent"+eventResponsibility.event+"'>"+eventResponsibility.responsibility+"-->"+eventResponsibility.event+" <i onclick='deleteResponsibility(\""+eventResponsibility.event+"\",\"methodExistingResponsibilityEvent\")' class=\"fas fa-trash\"></i></p>";
    console.log(eventResponsibilityText);
    $('#methodExistingResponsibilityEvent').append(eventResponsibilityText);
    responsibilitiesEventsRole[ responsibilitiesEventsRole.length] = eventResponsibility;

}
/**forming a json object for the role and using ajax to make a REST call which will
 * find and edit the method that role is in
 */
function editRole() {
    var isMandatory;
    if($('#methodRoleMandatory').is(':checked')){
        isMandatory=1;
    }
    else {isMandatory=0;}
    var role = {
        id:$("#methodRoleId").text(),
        name: $("#methodRoleName").text(),
        description: $("#methodRoleDescription").val(),
        isMandatory: isMandatory,
        checklist: getAndDivideTextInRows("methodRoleChecklist"),
        attachments: getAndDivideTextInRows("methodRoleAttachments"),
        requiredKnowledge: getAndDivideTextInRows("methodRoleRequiredKnowledge"),
        canBeCombinedWith:getAndDivideTextInRows("methodCanBeCombinedWith"),
        image:image,
        artifactsResponsibility: responsibilitiesArtifactsRole,
        eventsResponsibility:responsibilitiesEventsRole
    }
    var methodName = $("#methodName").text();
    console.log(image);
    $.ajax({
        type: "POST",
        url: "updateRole/"+methodName,
        headers: {
            "Content-Type": "application/json"
        },
        dataType:'json',
        data: JSON.stringify(role),
        success: function (data) {
            console.log("POST API RESPONSE::0"+data.name );
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
}

/**
 *
 * @param name the name of the element
 * @param text the id of the div from which we wanna remove an element
 */
function deleteResponsibility(name,text) {
    console.log("haha");
    var deletedElement;
    var element;
    if (text == "methodExistingResponsibilityArtifact") {
        element = document.getElementById(text);
        deletedElement = document.getElementById(text + name);
        deletedElement.remove();
        $.each(responsibilitiesArtifactsRole, function (index,value) {
            if (name == this.artifact) {
                responsibilitiesArtifactsRole.splice(index, 1);
            }
        });
    } else if (text == "methodExistingResponsibilityEvent") {
        element = document.getElementById(text);
        deletedElement = document.getElementById(text + name);
        deletedElement.remove();
        $.each(responsibilitiesEventsRole, function (index,value) {
            if (name == this.event) {
                responsibilitiesEventsRole.splice(responsibilitiesEventsRole.findIndex(x => x.event === name), 1);
            }
        });

    }
}
/**
 *
 * @param addedImage the image we wanna save/replace in the database
 */
function changeImageRole(addedImage){
    if (addedImage.files && addedImage.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#methodRoleImage').empty();
            image = e.target.result;
            console.log("lalalal");
            var images_div = "<img src=\"" + e.target.result + "\" height=\"200px\" width=\"400px\"\>";
            $('#methodRoleImage').append(images_div);
            console.log(image);
            image = e.target.result;
            console.log(image);

        }
        reader.readAsDataURL(addedImage.files[0]);
    }
}
function getAndDivideTextInRows(Id) {
    var plainText = document.getElementById(Id).value;
    var formattedText = [];
    var currentIndex = 0;
    var currentWord = "";
    for (var i = 0; i < plainText.length; i++) {
        if (plainText[i] != ";")
            currentWord += plainText[i];
        else {
            formattedText[currentIndex] = currentWord;
            currentIndex++;
            currentWord = "";
        }
    }
    return formattedText;
}
