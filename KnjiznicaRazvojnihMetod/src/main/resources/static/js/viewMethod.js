$(document).ready(function () {
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar, #content').toggleClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');

        var $el = $(this);
        $el.find('span').toggleClass('fa-times fa-angle-right');
        $el.toggleClass('sidebarCollapse');
    });
});