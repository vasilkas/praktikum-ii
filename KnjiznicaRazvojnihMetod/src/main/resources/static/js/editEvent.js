/** this function takes the id of an event and based on that finds the said event and displays it accordingly
 * in the modal which allows editing
 *
 * @param id
 */
function displayEventModal(id) {
        var checklist = []
        var goal = []
        var attachments = []
        $.each(method.events, function () {
            if(this.id==id){
            $('#methodEventName').text(this.name);
            $('#methodEventDescription').val(this.description);
            $('#methodEventDuration').val(this.duration);
            $('#methodEventType').val(this.type);
            $('#methodEventId').text(this.id);

            if (this.mandatory == true) {
                $("#methodEventMandatory").attr('checked', 'checked');
            } else {
                $("#methodEventOptional").prop("checked", true);
            }
            $.each(this.checklist, function () {
                checklist = checklist + this + ';';
            });
            $.each(this.attachments, function () {
                attachments = attachments + this + ';';
            });
            $.each(this.goal, function () {
                goal = goal + this + ';';
            });
            $('#methodEventGoal').val(goal);
            $('#methodEventChecklist').val(checklist);
            $('#methodEventAttachments').val(attachments);
        }
        });
        $('#eventModal').modal();
}
/**forming a json object for the artifact and using ajax to make a REST call which will
* find and edit the method that artifact is in
 */
function editEvent() {
    var isMandatory
    if($('#methodEventMandatory').is(':checked')){
        isMandatory=true;}
    else {isMandatory=false;}
    var event = {
        id:$("#methodEventId").text(),
        name: $("#methodEventName").text(),
        description: $("#methodEventDescription").val(),
        isMandatory: isMandatory,
        checklist: getAndDivideTextInRows("methodEventChecklist"),
        attachments: getAndDivideTextInRows("methodEventAttachments"),
        duration:$("#methodEventDuration").val(),
        type:$("#methodEventType").val(),
        goal:getAndDivideTextInRows("methodEventGoal")
    }
    var methodName = $("#methodName").text();
    $.ajax({
        type: "POST",
        url: "updateEvent/"+methodName,
        xhrFields: {
            withCredentials: true
        },
        headers: {
            "Content-Type": "application/json"
        },
        data: JSON.stringify(event),
        dataType:'json',
        success: function (data) {
            console.log("POST API RESPONSE::" + data.name);
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
}
