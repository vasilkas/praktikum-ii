package com.praktikum.SDM;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@SpringBootApplication
@PropertySources({
		@PropertySource("classpath:application.properties"),
		@PropertySource("classpath:auth0.properties")
})

public class SoftwareDevelopmentMethodsLibrary {
	public static void main(final String[] args) throws Exception {
		SpringApplication.run(SoftwareDevelopmentMethodsLibrary.class, args);
	}
}
