package com.praktikum.SDM.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.PersistenceConstructor;

import java.util.ArrayList;

/**
 *Artifact java class which represents an artifact
 *It is a a POJO class
 *
 * @version 1.0
 * @since   2020-05-06
 */
@Getter
@Setter
@NoArgsConstructor
public class Artifact {

    private String Id;
    private String description;
    private boolean isMandatory;
    private ArrayList<String> checklist;
    private String image;
    private ArrayList<String> attachments;
    private String name;

    public Artifact(String description, String name) {
        this.description = description;
        this.name = name;
    }

    @PersistenceConstructor
    public Artifact(String Id, String description, boolean isMandatory, ArrayList<String> checklist, String image, ArrayList<String> attachments, String name) {
        this.Id = Id;
        this.description = description;
        this.isMandatory = isMandatory;
        this.checklist = checklist;
        this.image = image;
        this.attachments = attachments;
        this.name = name;
    }
}
