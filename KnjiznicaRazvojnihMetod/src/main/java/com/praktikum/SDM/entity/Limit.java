package com.praktikum.SDM.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.PersistenceConstructor;

import java.util.ArrayList;

/**
 *Limit java class which represents a limit
 *It is a a POJO class
 *
 * @version 1.0
 * @since   2020-05-06
 */
@Getter
@Setter
public class Limit {

    private String Id;
    private String name;
    private String description;
    private ArrayList<String> advantages;

    @PersistenceConstructor
    public Limit(String id, String name, String description,ArrayList<String> advantages) {
        Id = id;
        this.name = name;
        this.description = description;
        this.advantages = advantages;
    }
}
