package com.praktikum.SDM.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.PersistenceConstructor;

/**
 *Steps is a  java class which represents a possible step in the scenario
 *It is a a POJO class
 *
 * @version 1.0
 * @since   2020-05-06
 */
@Getter
@Setter
@NoArgsConstructor
public class Steps {
    private int order;
    private String label;
    @JsonProperty("event")
    private String event;

    @PersistenceConstructor
    public Steps(int order, String label, String event) {
        this.order = order;
        this.label = label;
        this.event = event;
    }
}
