package com.praktikum.SDM.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 *User java class which represents a user who is registered with the site
 *It is a a POJO class
 *
 * @version 1.0
 * @since   2020-05-06
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Document(collection = "User")
@JsonInclude(JsonInclude.Include.NON_NULL)
/**
 *SDMethod is the user class which represents a user registered with our site
 * It is a POJO class
 *
 *
 * @version 1.0
 * @since   2020-05-06
 */
public class User {
    @Id
    private String Id;
    private String email;
    private String role;


    public boolean checkAccess() {
        if (this.role.equals("ADMIN"))
            return true;
        return false;
    }
}
