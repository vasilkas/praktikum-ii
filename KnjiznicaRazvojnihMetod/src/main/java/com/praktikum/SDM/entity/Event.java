package com.praktikum.SDM.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.PersistenceConstructor;

import java.util.ArrayList;

/**
 *Events java class which represents an event
 *It is a a POJO class
 *
 * @version 1.0
 * @since   2020-05-06
 */
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class Event {

    private String Id;
    private String description;
    private String type;
    private String duration;
    private ArrayList<String> goal;
    private boolean isMandatory;
    private ArrayList<String> checklist;
    private ArrayList<String> attachments;
    private String name;

    public Event(String description, String type, String name) {
        this.description = description;
        this.type = type;
        this.name = name;
    }
    @PersistenceConstructor
    public Event(String Id, String description, String type, String duration, ArrayList<String> goal, boolean isMandatory, ArrayList<String> checklist, ArrayList<String> attachments, String name) {
        this.Id = Id;
        this.description = description;
        this.type = type;
        this.duration = duration;
        this.goal = goal;
        this.isMandatory = isMandatory;
        this.checklist = checklist;
        this.attachments = attachments;
        this.name = name;
    }
}
