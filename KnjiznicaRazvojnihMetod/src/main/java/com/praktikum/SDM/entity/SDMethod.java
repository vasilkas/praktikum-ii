package com.praktikum.SDM.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.util.ArrayList;
import java.util.Date;
/**
 *SDMethod is the main java class which is used as an anchor for the other java classes
 * It is a POJO class
 *
 *
 * @version 1.0
 * @since   2020-05-06
 */
@Getter
@Setter
@Document(collection = "SDMethod")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SDMethod {

    @Id
    private String id;
    private String name;
    private ArrayList<String> access;
    private String description;
    private ArrayList<String> tags;
    private ArrayList<String> advantages;
    private ArrayList<String> disadvantages;
    private ArrayList<String> requirements;
    private ArrayList<String> images;
    private ArrayList<String> attachments;
    private ArrayList<String> internalLinks;
    private ArrayList<String> externalLinks;
    private ArrayList<Role> roles;
    private ArrayList<Event> events;
    private ArrayList<Scenarios> scenarios;
    private ArrayList<Artifact> artifacts;
    private ArrayList<Limit> limits;
    private Date date;

    public SDMethod(String name, String description, ArrayList<String> access, ArrayList<String> tags, ArrayList<String> advantages, ArrayList<String> disadvantages, ArrayList<String> requirements, ArrayList<String> images, ArrayList<String> attachments, ArrayList<String> internalLinks, ArrayList<String> externalLinks, ArrayList<Role> roles, ArrayList<Event> events, ArrayList<Scenarios> scenarios, ArrayList<Artifact> artifacts) {
        this.name = name;
        this.access = access;
        this.description = description;
        this.tags = tags;
        this.advantages = advantages;
        this.disadvantages = disadvantages;
        this.requirements = requirements;
        this.images = images;
        this.attachments = attachments;
        this.internalLinks = internalLinks;
        this.externalLinks = externalLinks;
        this.roles = roles;
        this.events = events;
        this.scenarios = scenarios;
        this.artifacts = artifacts;
    }

    /**
     * @param email - Viewers' email
     * @return true - email is found in access list
     *         false - email isn't found
     */
    public boolean hasAccess(String email){
        //needs updating
        //check if the email belongs to an ADMIN - if so return true
        if(email.equals("manager.sdml@gmail.com"))
            return true;
        if (access.contains(email))
            return true;
        return false;
    }

    /**
     * @param id - The id of the role
     * @return an int representing the index of the role
     */
    public int getIndexOfRoleById(String id){
        int index= 0;
        for(int i=0;i<roles.size();i++){
            if(roles.get(i).getId().equals(id)){
                index=i;
            }
        }
        return index;
    }
    /**
     * @param id - The id of the event
     * @return an int representing the index of the event
     */
    public int getIndexOfEventById(String id){
        int index= 0;
        for(int i=0;i<events.size();i++){
            if(events.get(i).getId().equals(id)){
                index=i;
            }
        }
        return index;
    }
    /**
     * @param id - The id of the artifact
     * @return an int representing the index of the artifact
     */
    public int getIndexOfArtifactById(String id){
        int index= 0;
        for(int i=0;i<artifacts.size();i++){
            if(artifacts.get(i).getId().equals(id)){
                index=i;
            }
        }
        return index;
    }
    /**
     * @param id - The id of the scenario
     * @return an int representing the index of the scenario
     */
    public int getIndexOfScenarioById(String id){
        int index= 0;
        for(int i=0;i<scenarios.size();i++){
            if(scenarios.get(i).getId().equals(id)){
                index=i;
            }
        }
        return index;
    }
    /**
     * @param id - The id of the limit
     * @return an int representing the index of the limit
     */
    public int getIndexOfLimitById(String id){
        int index= 0;
        for(int i=0;i<limits.size();i++){
            if(limits.get(i).getId().equals(id)){
                index=i;
            }
        }
        return index;
    }
}
