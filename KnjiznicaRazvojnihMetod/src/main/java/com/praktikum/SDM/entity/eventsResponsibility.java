package com.praktikum.SDM.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.PersistenceConstructor;

/**
 *eventsResponsibility java class which represents a responsibility an event may have
 *It is a a POJO class
 *
 * @version 1.0
 * @since   2020-05-06
 */
@Getter
@Setter
@NoArgsConstructor
public class eventsResponsibility {
    private String responsibility;
    private String event;
    @PersistenceConstructor
    public eventsResponsibility( String responsibility, String event) {
        this.responsibility = responsibility;
        this.event = event;
    }
}
