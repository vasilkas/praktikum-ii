package com.praktikum.SDM.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.PersistenceConstructor;

/**
 *artifactsResponsibility java class which represents a responsibility an artifact may have
 *It is a a POJO class
 *
 * @version 1.0
 * @since   2020-05-06
 */
@Getter
@Setter
@NoArgsConstructor
public class artifactsResponsibility {


    private String responsibility;
    private String artifact;

    @PersistenceConstructor
    public artifactsResponsibility(String responsibility, String artifact) {
        this.responsibility = responsibility;
        this.artifact = artifact;
    }
}
