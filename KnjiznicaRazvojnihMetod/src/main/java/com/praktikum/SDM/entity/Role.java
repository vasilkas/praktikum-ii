package com.praktikum.SDM.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.PersistenceConstructor;

import java.util.ArrayList;

/**
 *Role java class which represents a role
 *It is a a POJO class
 *
 * @version 1.0
 * @since   2020-05-06
 */
@Getter
@Setter
@NoArgsConstructor
public class Role {
    private String Id;
    private String name;
    private String description;
    private boolean isMandatory;
    private String image;
    private ArrayList<String> attachments;
    private ArrayList<String> requiredKnowledge;
    private ArrayList<String> canBeCombinedWith;
    private ArrayList<String> checklist;
    private ArrayList<com.praktikum.SDM.entity.artifactsResponsibility> artifactsResponsibility;
    private ArrayList<com.praktikum.SDM.entity.eventsResponsibility> eventsResponsibility;

    @PersistenceConstructor
    public Role(String Id, String description, boolean isMandatory, String image, ArrayList<String> attachments, ArrayList<String> requiredKnowledge, ArrayList<String> canBeCombinedWith, ArrayList<String> checklist, ArrayList<com.praktikum.SDM.entity.artifactsResponsibility> artifactsResponsibility, ArrayList<com.praktikum.SDM.entity.eventsResponsibility> eventsResponsibility, String name) {
        this.Id = Id;
        this.description = description;
        this.isMandatory = isMandatory;
        this.image = image;
        this.attachments = attachments;
        this.requiredKnowledge = requiredKnowledge;
        this.canBeCombinedWith = canBeCombinedWith;
        this.checklist = checklist;
        this.artifactsResponsibility = artifactsResponsibility;
        this.eventsResponsibility = eventsResponsibility;
        this.name = name;
    }
}
