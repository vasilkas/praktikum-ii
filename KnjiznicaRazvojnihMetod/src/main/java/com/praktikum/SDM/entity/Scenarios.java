package com.praktikum.SDM.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.PersistenceConstructor;

import java.util.ArrayList;


/**
 *Scenario is a java class which represents a scenario
 *It is a a POJO class
 *
 * @version 1.0
 * @since   2020-05-06
 */
@Getter
@Setter
@NoArgsConstructor
public class Scenarios {
    private String Id;
    private ArrayList<Steps> steps;
    private String name;

    public Scenarios(String name) {
        this.name = name;
    }
    @PersistenceConstructor
    public Scenarios(String Id, ArrayList<Steps> steps, String name) {
        this.Id = Id;
        this.steps = steps;
        this.name = name;
    }
}
