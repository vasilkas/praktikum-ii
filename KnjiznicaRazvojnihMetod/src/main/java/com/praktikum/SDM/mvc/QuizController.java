package com.praktikum.SDM.mvc;

import com.praktikum.SDM.dbservice.SoftwareDevelopmentMethodService;
import com.praktikum.SDM.entity.SDMethod;
import com.praktikum.SDM.security.TokenAuthentication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class QuizController {

    @Autowired
    SoftwareDevelopmentMethodService SDMService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @RequestMapping(value = {"/methodQuiz"}, method = RequestMethod.GET)
    protected String mqPage(final Model model, final Authentication authentication) {
        logger.info("Method Quiz page");

        if (authentication instanceof TokenAuthentication) {
            TokenAuthentication tokenAuthentication = (TokenAuthentication) authentication;
            model.addAttribute("profile", tokenAuthentication.getClaims());
        }
        List<SDMethod> methods = SDMService.getAll();

        model.addAttribute("methods",methods);
        // If not authenticated, still show home page.
        // View will render appropriate login/menu based on authentication status
        return "quiz";
    }
}
