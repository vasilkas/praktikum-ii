package com.praktikum.SDM.mvc;

import com.praktikum.SDM.dbservice.SoftwareDevelopmentMethodService;
import com.praktikum.SDM.entity.SDMethod;
import com.praktikum.SDM.security.TokenAuthentication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import java.util.ArrayList;
import java.util.List;

@Controller
public class AddMethodController {
    private Logger log = LoggerFactory.getLogger(SoftwareDevelopmentMethodService.class);

    @Autowired
    SoftwareDevelopmentMethodService SDMService;

    @RequestMapping(value="/addMethod",method=RequestMethod.GET)
    public String addMethodPage(Model model, final Authentication authentication){
        if (authentication instanceof TokenAuthentication) {
            TokenAuthentication tokenAuthentication = (TokenAuthentication) authentication;
            model.addAttribute("profile", tokenAuthentication.getClaims());
            model.addAttribute("token",tokenAuthentication);
        }
        List<SDMethod> methods = SDMService.getAll();
        model.addAttribute("methods",methods);
        return "addMethod";
    }

}
