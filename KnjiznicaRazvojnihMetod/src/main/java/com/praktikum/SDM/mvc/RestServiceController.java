package com.praktikum.SDM.mvc;

import com.praktikum.SDM.dbservice.SoftwareDevelopmentMethodService;
import com.praktikum.SDM.dbservice.UserService;
import com.praktikum.SDM.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.ArrayList;

import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;

@CrossOrigin(origins = "htpp://localhost:8080", maxAge = 3600)
@RestController
public class RestServiceController {
    private Logger log = LoggerFactory.getLogger(SoftwareDevelopmentMethodService.class);

    @Autowired
    SoftwareDevelopmentMethodService SDMService;
    @Autowired
    UserService userService;

    @RequestMapping(value = "/deleteMethod", method = RequestMethod.DELETE)
    protected ResponseEntity<Object> deleteMethod(HttpServletRequest request, @RequestBody String methodName) {
        SDMService.deleteSDMethodByName(methodName);

        return new ResponseEntity<>("Success", HttpStatus.MOVED_PERMANENTLY);
    }
    @RequestMapping(value = "/deleteUser", method = RequestMethod.DELETE)
    protected ResponseEntity<Object> deleteUser(@RequestBody String email) {
        userService.deleteUserByEmail(email);
        System.out.println("SUCCESS " + email);
        return new ResponseEntity<>("Success",HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:8080/postSDMethod")
    @RequestMapping(value="/postSDMethod", method= RequestMethod.POST)
    public ResponseEntity<Object> postSDMethod(@RequestBody SDMethod newMethod) {
        SDMService.saveNewMethod(newMethod);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }
    @RequestMapping(value = {"/method/updateArtifact/{methodName}"}, method = RequestMethod.POST)
    public  ResponseEntity<Object> updateArtifact(@RequestBody Artifact artifact,@PathVariable String methodName){
        SDMethod method = SDMService.findByName(methodName);
        SDMService.updateArtifacts(method,artifact);
        return new ResponseEntity<>(artifact, HttpStatus.OK);
    }
    @RequestMapping(value = {"/method/updateRole/{methodName}"}, method = RequestMethod.POST,produces = APPLICATION_JSON_VALUE)
    public  ResponseEntity<Object> updateRole(@RequestBody Role role,@PathVariable("methodName") String methodName){
        log.info(String.valueOf(role.isMandatory()));
        SDMethod method = SDMService.findByName(methodName);
        SDMService.updateRoles(method,role);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }
    @RequestMapping(value = {"/method/updateEvent/{methodName}"}, method = RequestMethod.POST)
    public  ResponseEntity<Object> updateEvent(@RequestBody Event event,@PathVariable String methodName){
        SDMethod method = SDMService.findByName(methodName);
        SDMService.updateEvents(method,event);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }
    @RequestMapping(value = {"/method/updateScenario/{methodName}"}, method = RequestMethod.POST)
    public  ResponseEntity<Object> updateScenario(@RequestBody Scenarios scenario,@PathVariable String methodName){
        log.info(scenario.getSteps().get(0).getEvent());
        SDMethod method = SDMService.findByName(methodName);
        SDMService.updateScenarios(method,scenario);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }
    @RequestMapping(value = {"/method/updateLimit/{methodName}"}, method = RequestMethod.POST)
    public  ResponseEntity<Object> updateLimit(@RequestBody Limit limit,@PathVariable String methodName){
        SDMethod method = SDMService.findByName(methodName);
        SDMService.updateLimits(method,limit);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }
    @RequestMapping(value = {"/method/updateDescription"}, method = RequestMethod.POST)
    public  ResponseEntity<Object> updateDescription(@RequestParam("description") String description,@RequestParam("methodName") String methodName){
        SDMethod method = SDMService.findByName(methodName);
        SDMService.updateDescription(method,description);
        return new ResponseEntity<>("SUccess", HttpStatus.OK);
    }
    @RequestMapping(value = {"/method/updateRequirements/{methodName}"}, method = RequestMethod.POST)
    public  ResponseEntity<Object> updateRequirements(@RequestBody String[] requirementsJson, @PathVariable("methodName") String methodName){
        ArrayList<String> requirements = new ArrayList<>();
        for (String data : requirementsJson) {
            requirements.add(data);
        }
        log.info(requirements.get(0));
        log.info(methodName);
        SDMethod method = SDMService.findByName(methodName);
        SDMService.updateRequirements(method,requirements);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }
    @RequestMapping(value = {"/method/updateAdvantages/{methodName}"}, method = RequestMethod.POST)
    public  ResponseEntity<Object> updateAdvantages(@RequestBody String[] advantagesJson, @PathVariable String methodName){
        ArrayList<String> advantages = new ArrayList<>();
        for (String data : advantagesJson) {
            advantages.add(data);
        }
        SDMethod method = SDMService.findByName(methodName);
        SDMService.updateAdvantages(method,advantages);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }
    @RequestMapping(value = {"/method/updateDisadvantages/{methodName}"}, method = RequestMethod.POST)
    public  ResponseEntity<Object> updateDisadvantages(@RequestBody String [] disadvantagesJson, @PathVariable String methodName){
        ArrayList<String> disadvantages = new ArrayList<>();
        for (String data : disadvantagesJson) {
            disadvantages.add(data);
            log.info(data);
        }
        SDMethod method = SDMService.findByName(methodName);
        SDMService.updateDisadvantages(method,disadvantages);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }
    @RequestMapping(value = {"/method/updateInternalLinks/{methodName}"}, method = RequestMethod.POST)
    public  ResponseEntity<Object> updateIntenalLinks(@RequestBody String[] internalJson, @PathVariable String methodName){
        ArrayList<String> internalLinks = new ArrayList<>();
        for (String data : internalJson) {
            internalLinks.add(data);

            log.info(data);
        }
        SDMethod method = SDMService.findByName(methodName);
        SDMService.updateInternalLinks(method,internalLinks);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }
    @RequestMapping(value = {"/method/updateExternalLinks/{methodName}"}, method = RequestMethod.POST)
    public  ResponseEntity<Object> updateExternalLinks(@RequestBody String[] externalJson, @PathVariable String methodName){
        ArrayList<String> externalLinks = new ArrayList<>();
        for (String data : externalJson) {
            externalLinks.add(data);
            log.info(data);
        }
        SDMethod method = SDMService.findByName(methodName);
        SDMService.updateExternalLinks(method,externalLinks);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }
}
