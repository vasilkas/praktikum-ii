package com.praktikum.SDM.mvc;
import com.praktikum.SDM.dbservice.SoftwareDevelopmentMethodService;
import com.praktikum.SDM.dbservice.UserService;
import com.praktikum.SDM.entity.SDMethod;
import com.praktikum.SDM.entity.User;
import com.praktikum.SDM.security.TokenAuthentication;
import com.praktikum.SDM.util.TokenUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sound.midi.Soundbank;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
@SuppressWarnings("unused")
@Controller
public class ProfileController {
    @Autowired
    UserService userService;
    @Autowired
    SoftwareDevelopmentMethodService SDMService;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    protected String profile(final Model model, final Authentication authentication) {
        // Since we've configured Spring Security to only allow authenticated requests to
        // reach this endpoint, and we control the Authentication implementation, we can safely cast.
        TokenAuthentication tokenAuthentication = (TokenAuthentication) authentication;
        if (tokenAuthentication == null) {
            return "redirect:/login";
        }
        List<SDMethod> methods = SDMService.getAll();
        List<User> users;
        String email = tokenAuthentication.getClaims().get("email").asString();
        User user = userService.findByEmail(email);
        if (user != null) {
            if (user.getRole().equals("EDITOR")) {
                for (SDMethod method : methods) {
                    if (!method.getAccess().contains(email))
                        methods.remove(method);
                }
                model.addAttribute("methods", methods);
            } else if (user.getRole().equals("ADMIN")) {
                users = userService.getAll();
                model.addAttribute("users", users);
                model.addAttribute("methods", methods);
            }
        }
        model.addAttribute("user", user);
        model.addAttribute("methods", methods);
        model.addAttribute("profile", tokenAuthentication.getClaims());
        model.addAttribute("token", tokenAuthentication);
        return "profile";
    }
}