package com.praktikum.SDM.mvc;

import com.praktikum.SDM.dbservice.SoftwareDevelopmentMethodService;
import com.praktikum.SDM.entity.SDMethod;
import com.praktikum.SDM.security.TokenAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class ViewMethodController {
    @Autowired
    SoftwareDevelopmentMethodService SDMService;

    @RequestMapping(value = {"/method/{name}"}, method = RequestMethod.GET)
    public String metoda(Model model, @PathVariable String name, final Authentication authentication) {
        if (authentication instanceof TokenAuthentication) {
            TokenAuthentication tokenAuthentication = (TokenAuthentication) authentication;
            model.addAttribute("profile", tokenAuthentication.getClaims());
        }
        List<SDMethod> methods = SDMService.getAll();
        SDMethod chosenMethod = SDMService.findByName(name);
        model.addAttribute("scrum",chosenMethod);
        model.addAttribute("methods",methods);

        return "method";
    }
}
