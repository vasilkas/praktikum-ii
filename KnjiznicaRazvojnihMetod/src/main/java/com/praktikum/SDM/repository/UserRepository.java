package com.praktikum.SDM.repository;


import com.praktikum.SDM.entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;


/**
 * UserRepository is a java interface used to access Users stored in MongoDB
 */
public interface UserRepository extends MongoRepository<User,String> {
    /**
     * @param email- users' email
     * @return User
     */
    @Query(value = "{ 'email' : ?0}")
    User findByEmail(String email);
    /**
     * @param email- users' email
     * @return User
     */
    @Query(value = "{ 'email' : ?0}", delete = true)
    void deleteByEmail(String email);
}