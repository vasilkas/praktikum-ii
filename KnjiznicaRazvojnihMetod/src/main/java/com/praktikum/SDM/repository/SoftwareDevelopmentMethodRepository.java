package com.praktikum.SDM.repository;

import com.praktikum.SDM.entity.SDMethod;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

/**
 * SoftwareDevelopmentMethodRepository is a java interface used to access SDMethods stored in the MongoDB
 */
public interface SoftwareDevelopmentMethodRepository extends MongoRepository<SDMethod,String> {
    /**
     *
     * @param name- the name of the method we want to retrieve
     * @return the method
     */
    @Query(value = "{ 'name' : ?0}")
    SDMethod findByName(String name);
    /**
     *
     * @param id the id of the artifact we want to retrieve
     * @return the artifact
     */
    @Query(value="{'artifacts.Id':?0}", fields = "{ 'artifacts.$': 1 }")
    SDMethod findByArtifactsID(String id);
    /**
     *
     * @param id - the id of the role we want to retrieve
     * @return the role
     */
    @Query(value="{'roles.Id':?0}", fields = "{ 'roles.$': 1 }")
    SDMethod findByRolesId(String id);
    /**
     *
     * @param id - the id of the event we want to retrieve
     * @return the event
     */
    @Query(value="{'events.Id':?0}", fields = "{ 'events.$': 1 }")
    SDMethod findByEventsId(String id);
    /**
     *
     * @param id - the id of the scenario we want to retrieve
     * @return the scenario
     */
    @Query(value="{'scenarios.Id':?0}", fields = "{ 'scenarios.$': 1 }")
    SDMethod findByScenariosId(String id);
    /**
     *
     * @param id - the id of the limit we want to retrieve
     * @return the limit
     */
    @Query(value="{'limits.Id':?0}", fields = "{ 'limits.$': 1 }")
    SDMethod findByLimitsId(String id);
    /**
     *
     * @param name the name of the method we want to delete
     *
     */
    @Query(value="{'name' : ?0}", delete = true)
    void deleteByName (String name);
}
