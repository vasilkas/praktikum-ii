package com.praktikum.SDM.dbservice;

import com.mongodb.MongoException;
import com.mongodb.MongoWriteException;
import com.praktikum.SDM.entity.User;
import com.praktikum.SDM.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * UserService is a java class which is used for performing
 * actions regarding the database "user-wise"
 *
 */
@Service
@EnableMongoRepositories(basePackageClasses = User.class)
public class UserService {

    private Logger log = LoggerFactory.getLogger(SoftwareDevelopmentMethodService.class);

    @Autowired
    UserRepository userRepo;

    public List<User> getAll() {
        try {
            List<User> users = userRepo.findAll();
            log.info("SDMService: EXECUTING getAll() s" + users.size());
            return users;
        } catch (MongoException e) {
            e.printStackTrace();
        }
        log.error("SDMService: getAll() FAILED\n\t null RETURNED");
        return null;
    }

    /**
     * @param email - users' email
     * @return User
     */
    public User findByEmail(String email) {
        try {
            User user = userRepo.findByEmail(email);
            log.info("SDMService: EXECUTING get(method)");
            return user;
        } catch (MongoException e) {
            e.printStackTrace();
        }
        log.error("SDMService: get(method) FAILED\n\t null RETURNED");
        return null;
    }

    /**
     * @param user - stores/updates User in database
     */

    public void update(User user) {
        try {
            userRepo.save(user);
        } catch (MongoWriteException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param email the email of the user we want to delete
     */
    public void deleteUserByEmail(String email){
        try {
            userRepo.deleteByEmail(email);
        } catch (MongoWriteException e) {
            e.printStackTrace();
        }
    }
}
