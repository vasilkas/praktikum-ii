package com.praktikum.SDM.dbservice;

import com.mongodb.MongoException;
import com.mongodb.MongoWriteException;
import com.praktikum.SDM.entity.*;
import com.praktikum.SDM.repository.SoftwareDevelopmentMethodRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

/**
 * SoftwareDevelopmentMethodService is a java class which is used for performing
 * actions regarding the database "method-wise"
 *
 */
@Service
@EnableMongoRepositories(basePackageClasses = SoftwareDevelopmentMethodRepository.class)
public class SoftwareDevelopmentMethodService {
    private Logger log = LoggerFactory.getLogger(SoftwareDevelopmentMethodService.class);
    @Autowired
    SoftwareDevelopmentMethodRepository SDMRepo;

    public List<SDMethod> getAll() {
        try {
            List<SDMethod> storedMethods = SDMRepo.findAll();
            log.info("SDMService: EXECUTING getAll() s" + storedMethods.size());
            return storedMethods;
        } catch (MongoException e) {
            e.printStackTrace();
        }
        log.info(Level.ERROR + "SDMService: getAll() FAILED\n\t null RETURNED");
        return null;
    }

    /**
     * @param name - the name od the desired method
     * @return the method
     */

    public SDMethod findByName(String name) {
        try {
            SDMethod users = SDMRepo.findByName(name);
            log.info(Level.INFO + "SDMService: EXECUTING get(method)");
            return users;
        } catch (MongoException e) {
            e.printStackTrace();
        }
        log.info(Level.ERROR + "SDMService: get(method) FAILED\n\t null RETURNED");
        return null;
    }

    /**
     * @param id - the id of the artifact we are looking for
     * @return the artifact as a part of an Json Object
     */
    public SDMethod findArtifactByID(String id) {
        try {
            return (SDMethod) SDMRepo.findByArtifactsID(id);
        } catch (MongoException e) {
            e.printStackTrace();
        }
        log.info(Level.ERROR + "SDMService: get(method) FAILED\n\t null RETURNED");
        return null;

    }

    /**
     * @param id - the id of the role we are looking for
     * @return the role as a part of an Json Object
     */
    public SDMethod findRoleByID(String id) {
        try {
            return (SDMethod) SDMRepo.findByRolesId(id);
        } catch (MongoException e) {
            e.printStackTrace();
        }
        log.info(Level.ERROR + "SDMService: get(method) FAILED\n\t null RETURNED");
        return null;

    }

    /**
     * @param id - the id of the event we are looking for
     * @return the event as a part of an Json Object
     */
    public SDMethod findEventByID(String id) {
        try {
            return (SDMethod) SDMRepo.findByEventsId(id);
        } catch (MongoException e) {
            e.printStackTrace();
        }
        log.info(Level.ERROR + "SDMService: get(method) FAILED\n\t null RETURNED");
        return null;

    }

    /**
     * @param id - the id of the scenario we are looking for
     * @return the scenario as a part of an Json Object
     */
    public SDMethod findScenarioByID(String id) {
        try {
            return (SDMethod) SDMRepo.findByScenariosId(id);
        } catch (MongoException e) {
            e.printStackTrace();
        }
        log.info(Level.ERROR + "SDMService: get(method) FAILED\n\t null RETURNED");
        return null;

    }

    /**
     * @param id - the id of the limit we are looking for
     * @return the limit as a part of an Json Object
     */
    public SDMethod findLimitByID(String id) {
        try {
            return SDMRepo.findByLimitsId(id);
        } catch (MongoException e) {
            e.printStackTrace();
        }
        log.info(Level.ERROR + "SDMService: get(method) FAILED\n\t null RETURNED");
        return null;

    }

    /**
     * @param method      -the method in which we wanna add/edit an artifact
     * @param changed-the new/edited artifact
     */

    public void updateArtifacts(SDMethod method, Artifact changed) {
        try {
            int deleteIndex = method.getIndexOfArtifactById(changed.getId());
            method.getArtifacts().remove(deleteIndex);
            method.getArtifacts().add(changed);
            SDMRepo.save(method);

        } catch (MongoWriteException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param method  - the method in which we wanna add/edit a role
     * @param changed -the new/edited role
     */
    public void updateRoles(SDMethod method, Role changed) {
        try {
            int deleteIndex = method.getIndexOfRoleById(changed.getId());
            method.getRoles().remove(deleteIndex);
            method.getRoles().add(changed);
            SDMRepo.save(method);
        } catch (MongoWriteException e) {
            e.printStackTrace();
            log.info("lalal");
        }
    }

    /**
     * @param method  - the method in which we wanna add/edit aa event
     * @param changed -the new/edited event
     */
    public void updateEvents(SDMethod method, Event changed) {
        try {
            int deleteIndex = method.getIndexOfEventById(changed.getId());
            method.getEvents().remove(deleteIndex);
            method.getEvents().add(changed);
            SDMRepo.save(method);
        } catch (MongoWriteException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param method  - the method in which we wanna add/edit a limit
     * @param changed -the new/edited limit
     */
    public void updateLimits(SDMethod method, Limit changed) {
        try {
            int deleteIndex = method.getIndexOfLimitById(changed.getId());
            method.getLimits().remove(deleteIndex);
            method.getLimits().add(changed);
            SDMRepo.save(method);
        } catch (MongoWriteException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param method  - the method in which we wanna add/edit a scenario
     * @param changed -the new/edited scenario
     */
    public void updateScenarios(SDMethod method, Scenarios changed) {
        try {
            int deleteIndex = method.getIndexOfScenarioById(changed.getId());
            method.getScenarios().remove(deleteIndex);
            method.getScenarios().add(changed);
            SDMRepo.save(method);

        } catch (MongoWriteException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param method      the method in which we wanna add/edit a description
     * @param description the new/edited description
     */
    public void updateDescription(SDMethod method, String description) {
        try {
            method.setDescription(description);
            SDMRepo.save(method);
        } catch (MongoWriteException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param method       -the method in which we wanna add/edit a requirement
     * @param requirements - the new/edited requirement
     */
    public void updateRequirements(SDMethod method, ArrayList<String> requirements) {
        try {
            method.setRequirements(requirements);
            SDMRepo.save(method);
        } catch (MongoWriteException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param method     the method in which we wanna add/edit advantages
     * @param advantages new/added advantages
     */
    public void updateAdvantages(SDMethod method, ArrayList<String> advantages) {
        try {
            method.setAdvantages(advantages);
            SDMRepo.save(method);
        } catch (MongoWriteException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param method        - the method in which we wanna add/edit disadvantages
     * @param disadvantages - new/added disadvantages
     */
    public void updateDisadvantages(SDMethod method, ArrayList<String> disadvantages) {
        try {
            method.setDisadvantages(disadvantages);
            SDMRepo.save(method);
        } catch (MongoWriteException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param method        - the method in which we wanna add/edit external links
     * @param externalLinks - the links we wanna add/edit
     */
    public void updateExternalLinks(SDMethod method, ArrayList<String> externalLinks) {
        try {
            method.setExternalLinks(externalLinks);
            SDMRepo.save(method);
        } catch (MongoWriteException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param method        - the method in which we wanna add/edit iternal links
     * @param internalLinks - the links we wanna add/edit
     */
    public void updateInternalLinks(SDMethod method, ArrayList<String> internalLinks) {
        try {
            method.setInternalLinks(internalLinks);
            SDMRepo.save(method);
        } catch (MongoWriteException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param method the new method we wanna add
     */
    public void saveNewMethod(SDMethod method) {
        try {
            SDMRepo.save(method);
        } catch (MongoWriteException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     *
     * @param name the name of the SDMethod we want to delete
     */
    public void deleteSDMethodByName(String name) {
        try {
            SDMRepo.deleteByName(name);
        } catch (MongoWriteException e) {
            e.printStackTrace();
        }
    }
}